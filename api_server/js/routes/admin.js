const express = require('express');
const router = express.Router();
const db = require('../lib/db');
const applications = require('../lib/applications');
const jobs = require('../lib/schedulerJobs');

//Handles the request for the dashboard data
var loadDashboard = function(){
    return new Promise(function(resolve, reject){

        //Fill this array with promises from our functions.
        var promises = [
            getSchedulerLogs(),
            getResourceLocatorErrors(),
            getDuplicateIdErrors()
        ];

        //When all our promises resolve
        Promise.all(promises).then(function(data){
            // data = [].concat.apply([], data);

            //Once we have the data, prep the viewModel
            var viewModel = prepareViewModel(data);
            return resolve(viewModel);
        }).catch(function(err){
            return reject(err);
        });
    });
};

var prepareViewModel = function(data){
    var viewModel = {
        jobLogs: data[0],
        resourceErrors: data[1],
        duplicateAppIdErrors: data[2],
    };
    return viewModel;
};

//Handles any get request made to '/'
router.get('/', function (req, res, next) {
    //Fetch the dashboard data
    loadDashboard().then(function(viewModel){
        res.render('dashboard', { model: viewModel });
    }).catch(function (err) {
        next(err);
    });
});


//Handles any get request made to '/refresh'
router.get('/refresh', function (req, res, next) {

    //Tell the scheduler to begin updating
    jobs.fullUpdate();

    res.statusCode = 200;
    res.end();
});


module.exports = router;