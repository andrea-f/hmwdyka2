var router = express.Router();
//var debug = require('debug')('How much do you know about?:server');
//var http = require('http');
//var fs = require('fs');
const api = require('../lib/db');
//var ip = require("ip");


router.get("/", isLoggedIn, function(req, res){
	/*
	 * Get all categories with items count
	 */
     console.log("Retrieving categories")
    api.getAllCategories().then(function (categories) {
        res.render('game', {
            categories:categories,
            user: req.user
        });
    })/*.catch(err) {
        err.status = 500;
        next(err);
    };*/

});


router.get("/:category/:key/:item/", isLoggedIn, function(req, res){
    /*
     * GET VIDEOS BY CATEGORY AND KEY AND ITEM
     */

    var category = decodeURI(req.params.category).trim(),
        key = decodeURI(req.params.key).trim(),
        item = decodeURI(req.params.item).trim(),
        server = "localhost",
        subkey;
    console.log("In category: "+category+" key: "+key + " item: "+item + " listening on: "+server);
    // Specific hacks for movies
    if ("attori" === key) {
        subkey = "nome";
    }
    api.getAllCategories().then(function (categories) {
        return api.getCategoryKeys(category).then(function (keys) {
            return api.getMappedReducedItems(category,key).then(function (items) {
                return api.getItems(category,key,item, subkey).then(function (videos) {
                    console.log("Retrieved "+videos.length+" for "+category+" with key "+key+" for item "+item);
                    res.render('game', {
                        categories:categories,
                        items: items,
                        category_name: category,
                        key_name: key,
                        keys: keys,
                        videos: videos,
                        item_name: item,
                        server: server,
                        user: req.user
                    });
                })
            })
        })
        
    })
    .catch(function (err) {
        err.status = 500;
        next(err);
    });
});


router.get("/:category/:key/", isLoggedIn, function(req, res){
    /*
     * GET VIDEOS BY CATEGORY AND KEY
     */

    var category = decodeURI(req.params.category).trim();
    var key = decodeURI(req.params.key).trim();
    console.log("In category: "+category+" key: "+key)
    api.getAllCategories().then(function (categories) {
        return api.getCategoryKeys(category).then(function (keys) {
            return api.mapReduceItems(category,key).then(function (items) {
                res.render('game', {
                    categories:categories,
                    items: items,
                    category_name: category,
                    key_name: key,
                    keys: keys,
                    user: req.user
                });
            })
        })
        
    })
    .catch(function (err) {
        err.status = 500;
        next(err);
    });
});


router.get("/:category/", isLoggedIn, function(req, res){
    /*
     * GET QUESTION BY CATEGORY
     */
    var category = decodeURI(req.params.category).trim();
    console.log("In category: "+category)
    api.getAllCategories().then(function (categories) {
        console.log("categories");
        console.log(categories);
        return api.getCategoryKeys(category).then(function (keys) {
            res.render('game', {
                categories: categories,
                keys: keys,
                category_name: category,
                user: req.user
            });
        })
    })
    .catch(function (err) {
        err.status = 500;
        next(err);
    });
});

// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
    return next();
    if (req.isAuthenticated())
        return next();
    res.redirect('/user/');
}

module.exports = router;
