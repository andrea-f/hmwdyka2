#!/usr/bin/env node
var express = require('express');
var router = express.Router();
var gameObject = require('../lib/Game');
var db = require('../lib/db');
var userFactory = require('../lib/User');
var User = userFactory.User;
var Game = gameObject.Game;
var games = {};
/*
 * Exposes Socket.io interface
 */
module.exports = function(io) {

  io.sockets.on('connection', function(socket){
    console.log("Socket " + socket.id + " connected.")
    // now broadcast the updated foo..
    io.sockets.emit('welcome', {
        "method": "welcome8888"
    }); 

    // START GAME
    socket.on("startGame", function(data) {
      // CHECK THAT USER EXISTS, OTHERWISE REDIRECT TO LOGIN PAGE
      console.log("Starting game...");
      /*if (data.cookie.length === 0) {
          io.sockets.emit('sendToLogin', {
            "url": "user"
          });          
      } */
      isRegistered(data).then(function (user, resolve, reject) {
          //console.log("user789", user)
          // TODO: User
          if ( user !== false ) { 
              var opts = {
                  "socket": socket,
                  "category":data.category
              };
              if (typeof data.key !== 'undefined' 
                  && typeof data.item !== 'undefined') {
                  opts["key"] = data.key;
                  opts["item"] = data.item;
              }
              opts.user = user;
              opts.socket = socket;
              // Create new game!  
              startNewGame(opts);
          } else {
            /*io.sockets.emit('sendToLogin', {
                "url": "user"
            });*/
            console.log("user false?", user)
          }
      })
    });

    // NEXT QUESTION  
    socket.on("nextQuestion", function(data) {
      var gameCode = data.gameCode;
      isRegistered(data).then(function (user, resolve, reject) {
        if (user !== false) { 
          console.log("Received: " + gameCode + " from " + socket.id);
          if(gameCode && games[gameCode]) {
              console.log("Asked next question for: "+gameCode + " from "+socket.id);
              games[gameCode].total_submitted_wrong_answers = 0;
              generateNewQuestion(socket);
          } else {
              console.log("Firing disconnect nextQuestion" + socket.id);
              socket.disconnect();
          }
        } else {
            /*io.sockets.emit('sendToLogin', {
                "url": "user"
            });*/
            console.log("user logged in?", user)
        };
      });
    });

    // CLIENT DISCONNECTED
    socket.on("disconnect", () => {
        // socket.gameCode must be set on the client
        var gs = activeGames(io.sockets.adapter.rooms, socket.gameCode);
        console.log("gs",gs)
        if (typeof socket.gameCode !== 'undefined') {
            try {
                socket.leave(socket.gameCode);
                console.log("Removing "+socket.id+" from "+socket.gameCode);
                var users = games[socket.gameCode].users,
                    current_users = [];

                for (var u; u < users.length; u++) {
                    if (users[u].socket_id !== socket.id) {
                        current_users.push(users[u]);
                    }
                }
                games[socket.gameCode].users = current_users;
                //socket.disconnect();
                var total_clients = gs.length;
                io.to(socket.gameCode).emit("clientDisconnected", total_clients);
                console.log("current_users: " + current_users.length + "total_clients: "+total_clients);
                
            } catch(err) {
                console.log("Error in removing element from connected clients array: "+err)
            }   
        } else {
            console.log("Disconnected here")
            socket.disconnect();
        }
    });

    // NEW ANSWER ARRIVED
    socket.on("submittedAnswer", function(data) {
        // Game socket
        console.log("submittedAnswer for "+socket.gameCode + " from "+socket.id);
        if(typeof data !== 'undefined' && socket.gameCode && games[socket.gameCode]) {
            // TODO: User
            // Check if user answer was correct
            //console.log("[play.js][submittedAnswer] Evaluating answer from "+data.user.name+"("+socket.id+") in game: "+socket.gameCode);
            //console.log("user received", data.user)
            resp = games[socket.gameCode].score({
              answer: data.answer,
              user: data.user
            })

            // Notify user whether their answer was correct or not
            socket.emit("answerEvaluated", {
                "correct":resp.correct,
                "user": resp.user
            })
            try {
                // Notify everyone else that an answer was submitted
                io.to(socket.gameCode).emit("answerEvaluatedFromAnotherUser", {
                    "correct": resp.correct,
                    // TODO: User
                    "user": resp.user.facebook.name
                })
            } catch (err) {
                console.log("[play.js][submittedAnswer] Error in notifying everyone of an answer: "+err);
            }
            // Move on to next question?
            if (resp.correct) {
                console.log('[play.js][submittedAnswer] answer is correct on '+socket.gameCode);
                games[socket.gameCode].total_submitted_wrong_answers = 0;
                generateNewQuestion(socket);
            } else {
                // Stay on this question
                games[socket.gameCode].total_submitted_wrong_answers +=1
                var total_wrong = games[socket.gameCode].total_submitted_wrong_answers;
                console.log("[play.js][submittedAnswer] TOTAL WRONG: "+total_wrong + " connected users: "+games[socket.gameCode].users.length);
                if (games[socket.gameCode].users.length === total_wrong) {
                    // All users have submitted their answer
                    generateNewQuestion(socket);
                    games[socket.gameCode].total_submitted_wrong_answers = 0;
                } 
                console.log('[play.js][submittedAnswer] wrong answer: ' + games[socket.gameCode].total_submitted_wrong_answers);
            }
        } else {
            console.log("Firing disconnect for" + socket.id)
            socket.disconnect();
        }
      });

      // CLIENT JOINED
      socket.on("clientConnected", function(client_data){
        var gameCode = client_data['gameCode'];
        //console.log("Client "+socket.id+" wants to join: "+gameCode);
        var g = games[gameCode];
        //console.log("client_data2", client_data)
        // if game code is valid...
        if (g.gameCode !== 'undefined') {
            // This will then be changed to something that stays persistant
            if (typeof client_data['user'] !== 'undefined') {
                var user = client_data['user'];
                //console.log(user)
                //console.log("Adding user to connected users: "+user.facebook.name);
                socket.join(g.gameCode);
                user.socket_id = socket.id;
                g.users.push(user)
                console.log("Informing everyone in "+g.gameCode+"  that client "+user.facebook.name+" has joined");
                io.to(g.gameCode).emit("clientJoined", io.nsps['/'].adapter.rooms[g.gameCode].users);
            } else {
            /*
                io.sockets.emit('sendToLogin', {
                    "url": "user"
                });
            */
                //console.log("clientConnected123", client_data);
            }             
            // Set client gameCode to the one of the game.
            socket.gameCode = g.gameCode;
            // Initialize the controller client
            socket.emit("connectedToGame", g);
        } else {
            console.log("disconnecting " + socket.id);
            socket.emit("fail", {});
            socket.disconnect();
        }
      })
  })

  /** 
   * @description Generates new question from db.
   */
  function generateNewQuestion(socket) {
    // Generate new question
    var gc = socket.gameCode;
    console.log("Asking new question on " + games[gc].category + " for: "+games[gc].gameCode);
    games[gc].getQuestion(games[gc].category).then(question => { 
        // Update game
        games[gc].question = question;
        // Update connected clients to the game
        // console.log("here123345", game.gameCode)
        console.log("Informing ", games[gc].gameCode, " of new question")
        //console.log(io.to(game.gameCode))
        socket.join(games[gc].gameCode);
        io.to(games[gc].gameCode).emit("nextQuestion", game);
        console.log("Notifying the user", socket.id)
        socket.emit("nextQuestion", game);
    })
  }

  /**
   * @description Checks if user is registered and connected.
   */
  function isRegistered(data) {
    return new Promise(function(resolve, reject) {
        var user = new User();
        user.setName("andrea");
        console.log("user111", user)
        resolve(user);
        /* Disable db check for now for session
        
        db.isConnectionFromKnownUser(data.cookie).then(function (user) {
              if ((user === null) || (typeof user === 'undefined')) {
                  io.sockets.emit('sendToLogin', {
                      "url": "user"
                  });
                  return reject(false);
              }
              return resolve(user);          
        });*/

    })
  }

  /**
   * @description Starts new game.
   */
  function startNewGame(data) {
      var game = new Game(),
          socket = data.socket;
      return game.startGame(data.category, data.key, data.item).then(function(game){
          try {
              // NEW GAME IS CREATED!
              games[game.gameCode] = game; 

              // UPDATE USER WITH NEW JOINED GAME
              //data.user.games = [game];

              // ADD USER TO GAME
              //console.log("data.user123", data.user)
              game.users.push(data.user);

              // Store the game socket
              socket.gameCode = game.gameCode;
              socket.game = game;

              // CREATE ROOM WITH GAMECODE
              socket.join(game.gameCode);

              // Inform server that game has started
              io.to(game.gameCode).emit("nextQuestion", game)
          } catch (err) {
              console.log(err)
          }
      });
  }

  //http://stackoverflow.com/questions/17966300/socket-io-how-to-get-room-id
  /**
   * @description Shows active games
   */
  function activeGames(object, gameCode) {
      if ( !(object && typeof object === 'object') ) {
        return null;
      }
      var result = [];
      for (var key in object) {
        if ((object.hasOwnProperty(key)) && (gameCode === key)) {
          var roomInfo = {}
          roomInfo[key] = {};
          for (var subkey in object[key]) {
              if (object[key].hasOwnProperty(subkey)) {
                  roomInfo[key][subkey] = object[key][subkey];
              }
          }
          result.push(roomInfo);
        }
      }
      return result;
  }
};

return module.exports;
