//var express = require('express');
var router = express.Router();

var debug = require('debug')('How much do you know about?:server');

var http = require('http');
var fs = require('fs');
const api = require('../lib/getQuestions');

var socketCodes = {}

router.put("/qs/:category/:question_number/:format*?", function(req, res){
	/*
	 * GET QUESTION BY CATEGORY
	 */
	var connector = decodeURI(req.params.connector).trim();
	var category = decodeURI(req.params.category).trim();
	var question_number = parseInt(decodeURI(req.params.question_number).trim());
	var format = decodeURI(req.params.format).trim();
	//console.log(category)
	api.getQuestions.run(category).then(function (questions) {
        //applications = [].concat.apply([], applications);
        // questions is a list of objects
        console.log("Retrieved questions: "+questions.length)
        //console.log(questions)
        
        var viewModel = {
          questions: questions,
          category: category,
          total_questions: questions.length,
          question_number: question_number

        };
        //console.log(viewModel)
        

        //res.render('question', viewModel);
        if (connector==="game"){
        	var gameCode = crypto.randomBytes(3).toString('hex');
         
	        // Ensure uniqueness
	        while(gameCode in socketCodes) {
	            gameCode = crypto.randomBytes(3).toString('hex');
	        }
	         
	        // Store game code -> socket association
	        socketCodes[gameCode] = listener.sockets.sockets[socket.id];
	        socket.gameCode = gameCode
	         
	        // Tell game client to initialize 
	        //  and show the game code to the user
	        req.io.sockets.emit('initialize', questions[0].url); 
        } else {
        	req.io.sockets.emit('update', questions[0]); 
        	res.render('question', {
                model:viewModel
            });
        }

        if (format === "json") {
        	res.json(viewModel)
        } else {
        	res.render('question', {
                model:viewModel
            });
        }
                
    })
    .catch(function (err) {
        err.status = 500;
        next(err);
    });

});


router.put("/:category/", function(req, res){
    /*
     * GET QUESTION BY CATEGORY
     */
    // "category_of_event": "/film/film/", 

    // var connector = decodeURI(req.params.connector).trim();
    var category = decodeURI(req.params.category).trim();
    api.getQuestions.run(category).then(function (questions) {
        res.render('question', {
            model:viewModel
        });           
    }).catch(function (err) {
        err.status = 500;
        next(err);
    });

});


module.exports = router;
