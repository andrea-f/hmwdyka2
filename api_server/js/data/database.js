var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var Phrase = new Schema({
    phrase : String,
    image : String,
    language: String,
    submitted: String
});

mongoose.model('phrase', Phrase);
//mongoose.connect('mongodb://localhost:27017/');
