const api = require('./getQuestions');
var crypto = require('crypto');

/**
 * @description Game class.
 */
var Game = function () {
  	this.gameCode = "";
  	this.level = 0;
  	this.question = {};
	this.socket_codes = {};
	this.video_start_time = 20;
  	this.levels = [
		{
			'name':'easy',
			'score':0
		},
		{
			'name':'medium',
			'score':1
		},
		{
			'name':'hard',
			'score':2
		}
	];
	this.start_time = 0;
	this.end_time = 0;
	this.question_run_time = 0;
	this.connected_clients = [];
	this.total_questions = 0;
	this.max_questions = 0;
	//this.current_users = [];
	this.total_submitted_wrong_answers = 0;
	this.category = "Nocat";
	this.gameName = "";
	this.key = "";
	this.item = "";
	this.users = [];
	this.questions = [];
};

/**
 * @description Updates a user score.
 */
Game.prototype.score = function(data) {
	var answer = data.answer,
		// TODO: User
		user = data.user || {},
		position = this.question.correct,
		correct = false;
	// TODO: User
	try {
		user.total_answered_questions += 1;
	} catch (err) {
		user.total_answered_questions = 1;
		user.score = 0;
		user.correct_questions = [];
	}
	if (answer === position) {
		user.score += 1 + this.levels[this.level].score;
		user.correct_questions.push(this.question);
		this.question_run_time = 0;
		correct = true;
	} else {
		user.score -= this.levels[this.level].score;
	};
	return {
		user: user,
		correct: correct
	}
};

/**
 * @description Generates random game code.
 */
Game.prototype.getCode = function () {
  // Generate a code
  var gameCode = crypto.randomBytes(3).toString('hex');
  // Ensure uniqueness
  while(gameCode in this.socket_codes)
  {
    gameCode = crypto.randomBytes(3).toString('hex');
  }
  this.gameCode = gameCode;
  return gameCode;
}

// Only load videos which match a specific subset
var mongo = require('mongodb'),
	monk = require('monk'),
	db = monk('db:27017/items');

/**
 * @description Fetches videos from DB.
 * @param {string} category 
 * @param {string} key 
 * @param {string} item 
 */
function getVideosDB(category, key, item) {
  'use strict';
  var collection = db.get(category);
  console.log("Looking for videos of: "+category+" key "+key+" item "+item);
  return new Promise(function(resolve, reject) {
  	var f = {};
  	if ((typeof key !== 'undefined') && (typeof item !== 'undefined')) {
  		var k;
  		if ("matches" === key) {
  			k = "query";
  			var expression = ".*" + item + ".*";
    		var rx = new RegExp(expression, 'i');
  			f[k] = rx;
  		} else if ("attori" === key) {
  			k = "item.attori.nome";
  			f[k] = item;
  		} else {
	  		k = "item."+key;
  			f[k] = item;
  		}
  	}
    collection.find(f,{},function(e,videos){
        if (e) {
          console.log(e);
          return reject(e);
        }
        console.log("Total videos retrieved: "+videos.length);
        /*for (var i = 0; i < videos.length; i++) {
        	console.log(videos[i].item.club)
        }*/
        return resolve(videos);
    });
  });
}

/**
 * @description Fetches new question from BD.
 */
Game.prototype.getQuestion = function (category,key,item) {
	if (typeof category === 'undefined') {
		console.log("No category provided starting movies game.");
		return "";
	}
	//var game_ref = this;
	this.total_questions +=1;
	this.category = category;
	if (typeof key === 'undefined') {
		key = this.key;
	} 
	if (typeof item === 'undefined') {
		item = this.item;
	}
	return getVideosDB(category,key,item).then(function (videos) {
		console.log("Retrieved number of videos: " + videos.length);
		return api.getQuestions.run(videos,category,key).then(function (questions) {  
			console.log("Back in game with question:");
			//console.log(questions[0]xf)
	        return questions[0];
		}).catch(err=>{
			console.log(err);
			return err;
		})
	}).catch(function (err) {
	    err.status = 500;
		console.log(err);
		return err;
	});
}

/**
 * @description Starts game based on input parameters.
 */
Game.prototype.startGame = function (category, key, item) {
	game = this;
	game.gameName = category + ((typeof key !== 'undefined') && (typeof item !== 'undefined')) ? "_" + key + "_"+ item : "";
	console.log(game.gameName);
	game.key = key;
	game.item = item;
    return this.getQuestion(category,key,item).then(function (question) {            
        game.start_time = Date.now();
        game.question = question;
        var gameCode = game.getCode();      
        return game;
    })
    .catch(function (err) {
		err.status = 500;
		console.log("err startGame", err);
        return err;
        
    });         
}

Game.prototype.endGame = function () {
	this.socket = "";
	this.end_time = Date.now();
}

module.exports = {
  Game: Game
};