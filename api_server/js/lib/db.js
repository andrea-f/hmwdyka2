// Only load videos which match a specific subset
var mongo = require('mongodb'),
  monk = require('monk'),
  db = monk('db:27017/items'),
  //dbUsers = monk('localhost:27017/passport'),
  mongojs = require('mongojs'),
  ObjectId = mongo.ObjectID;

/**
 * @description fetches videos from DB
 */
function getVideosDB(category) {
  'use strict';
  var collection = db.get(category);
  console.log("Looking for videos of: "+category);
  return new Promise(function(resolve, reject) {
    collection.find({},{},function(e,videos){
        if (e) {
          console.log(e);
          return reject(e);
        }
        console.log("Total videos retrieved: "+videos.length);
        return resolve(videos);

    });
  });
}

/**
 * @description Fetches top items in category.
 */
function mapReduceItems(category, key) {
  'use strict';
  console.log("In mapReduceItems for "+category + " key: "+key)
  var hasKey;

  if (typeof key === 'undefined') {
      hasKey = false;
  } else {
      hasKey = true;
      // PROD RUN:
      // var db_client = mongojs('db:27017/items', [category, category + '_' + key + '_results']);
      var db_client = mongojs('items', [category, category+'_'+key+'_results']);
      var mapper = function () { 
          if ((this.item[key] instanceof Array) === true) { 
              for (var c = 0; c < this.item[key].length; c ++) {
                  var item = this.item[key][c];
                  // Specific for movies
                  if (typeof item === 'object') {
                     item = item['nome']
                  }
                  emit(item, 1);
              }
          } else {
              var item = this.item[key];
              emit(item, 1);
          }
      };
       
      var reducer = function(item, count){
        return Array.sum(count);
      };

      db_client[category].mapReduce(
          mapper,
          reducer,
          {
            out : category+'_'+key+'_results',
            scope : {
              "key": key
            }
          }
      );
  }
  return new Promise(function(resolve, reject) {
      if (hasKey === true) {
          db_client[category+'_'+key+'_results'].find().sort({ "value": -1 }, function (e, docs) {

            if (e) {
              console.log(e);
              return reject(e);
            }
            var keys = []
            for (var o=0; o < docs.length; o++) {
              var entry = {
                "name": docs[o]['_id'],
                "count": docs[o]['value']
              }
              if (keys.length <= 30) {
                  keys.push(entry);  
              } 
            }
            return resolve(keys);
        });
      } else {
          return reject("No key supplied.");
      }
  })
}

/**
 * @description Fetch items from DB.
 */
function getItems(category, key, item, subkey) {
    'use strict';
    return new Promise(function(resolve, reject) {
        var k;
        // Workaround for film
        if (typeof subkey !== 'undefined') {
          k = "item."+key+"."+subkey;
        } else {
          k = "item."+key;
        }
        console.log(k, item);
        var f = {};
        f[k] = item;
        db.get(category).find(f, 
          function(e, docs) {
            if (e) {
                console.log(e)
                return reject(e)
            }
            return resolve(docs);
        })
    })
}

/**
 * @description Fetches keys from DB.
 * @param {strign} category 
 * @param {string} key 
 */
function getMappedReducedItems(category, key) {
    'use strict';
    return new Promise(function(resolve, reject) {
        db.get(category+'_'+key+'_results').find({}, {sort:{"value": -1}}, function(e, docs) {
            var keys = [];
            for (var o=0; o < docs.length; o++) {
              var entry = {
                "name": docs[o]['_id'],
                "count": docs[o]['value']
              }
              if (keys.length <= 30) {
                  keys.push(entry);
              }
            }
            return resolve(keys);
        })
    })
}

/**
 * @description Gets the keys from a specific categories.
 */
function getCategoryKeys(category) {
    'use strict';
    return new Promise(function(resolve, reject) {
        console.log("getCategoryKeys")
        db.get(category).findOne({}, function (e,record) {
            var item = record.item,
                keys = [];
            for (var key in item) {
                keys.push(key);
            }
            return resolve(keys);
        });
    })
}

/**
 * @description Fetches all categories from db.
 */
function getAllCategories() {
  'use strict';
  return new Promise(function(resolve, reject) {
      var categories = ["football","videogames","cartoons", "film","music"];
      var categories_stats = [];
      function fillCategoriesData(i) {
        if (i !== -1) {
            var name = categories[i];
            db.get(name).count({}, function (error, count) {
                var category = {
                  "name":name,
                  "count":count
                }
                categories_stats.push(category);                
                return fillCategoriesData(i - 1);
            });
          
        } else {
          return resolve(categories_stats);
        }
      }
      return fillCategoriesData(categories.length-1);
  })
}

/**
 * @description Checks if the user has authed.
 * @param {string} cookie 
 */
function isConnectionFromKnownUser(cookie) {
  'use strict';
  console.log(cookie)
  return new Promise(function(resolve, reject) {
      // Select collections from mongodb
      var db_client = mongojs('passport', ['sessions','users']);

      // Get token from cookie
      var token = decodeURIComponent(cookie).split('s:')[1].split('.')[0];
      //console.log("token: "+token);

      // Check if token is in sessions database
      db_client['sessions'].findOne({"_id":token}, function (e, docs) {
        if (e) {
            console.log(e)
            return reject(e);
        }

        // If it is, then from the match get the user id
        try {
          var user_id = JSON.parse(docs.session).passport.user;
          //console.log("Matched user id: "+user_id)
        } catch (err) {
          console.log(err)
          console.log("Redirecting user to login")
          return resolve(false);
        }
        // With the user id grab the user deatils
        db_client['users'].findOne({"_id":ObjectId(user_id)}, function (e, user) {
            if (e) {
              console.log(e)
              return reject(e);
            }
            //console.log("Retrieved:")
            //console.log(user)
            return resolve(user);
        })
        
      })
      
  })
}

module.exports = {
  getVideosDB:getVideosDB,
  getAllCategories: getAllCategories,
  mapReduceItems: mapReduceItems,
  getCategoryKeys: getCategoryKeys,
  getItems: getItems,
  getMappedReducedItems: getMappedReducedItems,
  isConnectionFromKnownUser: isConnectionFromKnownUser
};