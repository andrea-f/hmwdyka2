var User = function (name) {
	this.name = name;
	this.correct_questions = [];
	this.game_codes = [];
	this.total_answered_questions = 0;
	this.current_game = "";
	this.score = 0;
	this.socket_id = "Not connectedd";
	this.facebook = {
		name: name
	}
};

User.prototype.setName = function (name) {
	this.name = name;
	this.facebook.name = name;
}

module.exports = {
	User: User
};
