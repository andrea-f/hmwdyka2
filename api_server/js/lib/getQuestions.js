var getQuestions = getQuestions || {};

getQuestions.videos = [];
/**
 * @description Executes question generator.
 */
getQuestions.run = function(videos,category, key) {
  try {
    this.category = category;
    this.videos = videos;
    return new Promise(function(resolve, reject) {
      return getQuestions.generateQuestions(category, key).then(
        function (questions) {
          //console.log("questions", questions);
          resolve(questions);
        })
      .catch(function (err) {
              err.status = 500;
              reject("[getQuestions.run] "+err.stack)
      }) 
    });
  } catch (err) {
    console.log(err)
  } 
}


/**
 * @description Wrapper of get videos.
 */
getQuestions.getVideos = function() {
  return getQuestions.videos;
}

// FILM
// FOOTBALL
// VIDEOGAMES
// "value"
// "title"
// "date"
// "author"
// "country"
// "gender"
// "start_date"
getQuestions.types = require('./types.json');


var alreadySelected = alreadySelected || [];

/**
 * @description Generates questions recursively
 */
getQuestions.generateQuestions = (category, key) => {
  'use strict';
  return new Promise((resolve, reject) => {
    // Check that user entered category exists...
    var types = getQuestions.types[category];
    if (typeof types === 'undefined') {
      return reject("Category " + category + " not available.");
    }
    var max_options = 3,
        max_questions = 1,
        q_type = "",
        parameters = {},
        questions = [];
    function questionType() {
      var q_type = getRandom(types.length, 0);
      if (types[q_type].type === key) {
        return questionType();
      }
      return q_type;
    }
    try {
      for (var g = 0; g < max_questions; g ++) {
          // Get question type
          q_type = questionType();
          parameters = {
            "type":types[q_type].type, 
            "phrase":types[q_type].phrase,
            "max_options": max_options,
            "total_videos": getQuestions.getItem()
          };

          if (typeof types[q_type].subtype !== 'undefined') {
            parameters.subtype=types[q_type].subtype;
          }
          // Question is actually created here
          questions.push(getQuestions.makeQuestion(parameters));
      }
      return resolve(questions);
    
    } catch (err) {
      console.log("err getQuestions.generateQuestions", err)
      return reject(err)
    }

  });
};


var vds = {};
/**
 * @description Gets items
 */
getQuestions.getItem = function(opt, type, subtype) {
  'use strict';
  // Try to load videos if there arent any.
  var videos = this.getVideos(this.category);
  vds[this.category] = videos;
  if ((typeof opt === 'undefined') || (typeof vds[this.category][opt] === 'undefined')) {
    return vds[this.category].length;
  }  
  //console.log("hereee", vds[this.category][opt], this.category, opt)
  var answer,
      selection = vds[this.category][opt].item[type];
  if (typeof selection === 'string') {
    answer = selection;
  } else if ((selection instanceof Array) === true) {
      var entry = getRandom(selection.length, 0);
      if (typeof subtype !== 'undefined') {
        console.log(opt + " " + type + " " + entry + " " + subtype);
        console.log(selection[entry][subtype]);
        if (typeof selection[entry][subtype] === 'string') {
            answer = selection[entry][subtype];
        } 
      } else {
          answer = selection[entry];
      }; 
  } else if ((selection instanceof Object) === true) {
      answer = selection[subtype];
  }
  return {
    "answer": answer,
    "item": vds[this.category][opt]
  };
}


/**
 * @description Makes a specific question.
 */
getQuestions.makeQuestion = function(questionParameters){
    'use strict';
    var type = questionParameters['type'],
        phrase = questionParameters['phrase'],
        max_options = questionParameters['max_options'],
        total_videos = questionParameters['total_videos'];
    if (typeof questionParameters['subtype'] !== 'undefined') {
      var subtype = questionParameters['subtype'];
    }
    if (typeof max_options === 'undefined') {
      max_options = 3;
    }
    var max = total_videos-1,
        min = 0;
    
    // Create multiple choice
    //var multiple_choices = ["" * max_options];
    var multiple_choices = [];
    for(var i=0; i<=max_options; ++i) multiple_choices.push("");
    // Create right answer
    var position = getRandom(max_options+1,0);

    // Gets reference video to use for question
    var reference = getReference(max, min, type);
    
    //Populate phrase vars
    try {
        phrase = phrase.replace(':X:',reference.item.item.club).replace(':Y:',reference.item.date);
    } catch (err) {
        try {
          phrase = phrase.replace(':X:',reference.item.item.title);
        } catch (err) {
          console.log(err);
        }
    }
    
    // Set right answer position, so we can check later if correct
    multiple_choices[position] = reference.answer;
    
    // Generate multiple choices matrix
    try {
      addAnswers(max_options, multiple_choices, type, subtype, position, max, min);
    } catch (err) {
      console.log("addAnswers first call err", err);
    }
    //console.log("right answer456", position, reference.answer)
    return {
      "url": reference.item.url,
      "type": type,
      "question": phrase,
      "answers":multiple_choices,
      "correct": position
    }
}


/** 
 * @description Select a random entry in the list of videos
 * @description Which has not already been selected from the current list
 * @param {Number} max
 * @param {Number} min
 */
function getReference(max, min, type, subtype) {
  var
    num = getRandom(max, min),
    obj = getQuestions.getItem(num, type, subtype),
    item = obj.item;
  if (alreadySelected.indexOf(item.url) === -1) {
    alreadySelected.push(item.url);
    return obj;
  } else {
    return getReference(max, min, type, subtype);
  }
}


/**
 * @description Remove unnecessary word.
 * @param {String} word 
 */
function isUnwanted(word) {
  var unwanted_chars = ['#', '=', ';'];
  for (var u = 0; u < unwanted_chars.length; u++) {
    if (!(word.indexOf(unwanted_chars[u]) !== -1)) return false;
  }
}


/*
 * This function fetches one item from the videos array.
 * After a random video has been selected, 
 * Select other max_options options based on question type
 * Create array with other answers and real item
 * Return object with:
 *  {
 *    "url":reference.url,
 *    type:{
 *      "question": phrase,
 *      "answers":multiple_choices,
 *      "correct": position
 *    }
 *  }
 */
function getRandom(max, min) {
  'use strict';
  return Math.floor((Math.random() * max) + min);
};


/**
 * Populates other answers for the question, wrong answers.
 * @param {Number} maxOptions 
 * @param {Array} multipleChoices 
 * @param {String} type 
 * @param {String} subtype 
 * @param {Number} correctPosition 
 * @param {Number} max
 * @param {Number} min
 */
function addAnswers(maxOptions, multipleChoices, type, subtype, correctPosition, max, min) {
    try {
        // Are there more answers to fill multiple choices?
        var
          next = 0,
          currentIteration = 0,
          maxIterations = 200;
        
        while (maxOptions >= 0) {
          if (currentIteration === maxIterations) {
            maxOptions = -1;
          }
          // Get a random entry:
          var opt = getRandom(max, min);
          var obj = getQuestions.getItem(opt, type, subtype);
          //console.log("here:::", obj.answer, multipleChoices, maxOptions, correctPosition)
          if ((multipleChoices.indexOf(obj.answer) === -1) &&
            (typeof obj.answer !== 'undefined') &&
            (isUnwanted(obj.answer) === false)) {
            for (var emptyIndex = 0; emptyIndex < multipleChoices.length; emptyIndex++) {
              if ((multipleChoices[emptyIndex].length === 0) && (emptyIndex !== correctPosition)) {
                multipleChoices[emptyIndex] = obj.answer;
                next = 1;
                break;
              }
            }
          } else {
            next = 0;
          }
          //console.log("in while", maxOptions, next, multipleChoices, type, subtype, correctPosition)
          //return addAnswers(maxOptions - next, multipleChoices, type, subtype, correctPosition, max, min);
          maxOptions = maxOptions - next;
          var filled = 0;
          for (var i=0; i < multipleChoices.length; i++) {
            if (multipleChoices[i].length > 0) {
              filled += 1;
            }
          }
          if (filled >= multipleChoices.length) {
            maxOptions = -1;
          }
          currentIteration += 1;
        }
        return multipleChoices;
        
    } catch (err) {
        //console.log(multipleChoices);
        console.log("addAnswers err", err);
        
        return multipleChoices;
    }
}


module.exports = {
  getQuestions: getQuestions
};