//require("./js/data/database.js");

express = require('express');
jade = require('jade');
var app = express();


var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');
var session      = require('express-session');

var configDB = require('./js/config/database.js');

// configuration ===============================================================
mongoose.connect('db:27017'); // connect to our database

var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var path = require('path');

app.set('views', path.join(__dirname, './js/views'));
app.set('view engine', 'jade');
//app.use('/static', express.static(__dirname + './js/views/static'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// required for passport, before passport is initialised
var MongoStore = require('connect-mongo')(session);
app.use(session({
    secret: 'ilovescotchscotchyscotchscotch', // session secret
    resave: true,
    saveUninitialized: true,
    store: new MongoStore({ 
      mongooseConnection: mongoose.connection, 
      ttl: 14 * 24 * 60 * 60  
    }), 
    cookie: { secure: false, httpOnly: false },
    name: "hmdyka"
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

app.use(express.static('./clients'));

require('./js/config/passport')(passport); // pass passport for configuration

var io = require('socket.io').listen(app.listen(3000));

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));


var r = require('./js/routes/quiz'),
    a = require('./js/routes/authentication');
    
require('./js/routes/play')(io);

//app.use('/user/',  a);
app.use('/quiz/', r);



app.use(cookieParser());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
  	res.status(err.status || 500);
  	res.render('error', {
  	  message: err.message,
  	  error: err
  	});
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  //res.json({ error: err.message })
  res.render('error', {
      message: err.message,
	    error: {}
  });
});


module.exports = app;
