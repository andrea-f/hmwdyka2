/**
 * @class init
 * @description Start up the application, by applying any AMD config
 * and calling in the first dependency.
 */
(function() {
	"use strict";
	window._config = {
		'baseUrl': 'js/',
		'paths': {
			text: 'libs/require-text'
		}
	};

	window.requirejs.config(window._config);

	// start the app
	require(['bootstrap'], function(bootstrap) {
		bootstrap.init();
	});
})();