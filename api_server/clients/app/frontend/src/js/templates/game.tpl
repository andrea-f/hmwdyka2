
  <div class="row" id="mainarea">
    <div class="col-sm-14">

      	<div class="row" id="playerrow">
			<div class="embed-responsive embed-responsive-16by9" id="playerembed">
				<div id='player' class="embed-responsive-item"></div>
			</div>
			<h4 class="text-nowrap">
		    	<div id="creator"></div> | <div id="gameCode" style="display:inline-block;"></div>
		    	<div id ="totalquestions" style="display:inline-block;"></div>
		    	<div id ="connectedclients">Game not started</div>
		    	<div id ="connecttogame">Game link will be shown here</div>
	      	</h4>
		</div>
		<div class="row" id='row-singleplayer'>
			<div class="embed-responsive embed-responsive-16by9" id="singleplayer-embed">
				<div id='singleplayer' class="embed-responsive-item"></div>
			</div>
		</div>

    </div>
  </div>
