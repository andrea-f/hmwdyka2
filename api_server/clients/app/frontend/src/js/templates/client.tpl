<div class="column center-block">
	<div class="row">
		<section >
			
			<div class="box">
				<div class="questions" id="questions">
				</div>

			</div>
		</section>
	</div>
	<div class="row">
		<section>
			
			<div class="box">
				
				<button class="btn-block btn-lg btn btn-primary" id="nextquestion">
					Next Question
				</button>
			</div>
		</section>
	</div>
	<div class="row">
		<section>
			<br>
			<div class="box">
				
				<button class="btn-block btn-lg btn btn-primary" id="newgame">
					New Game
				</button>
			</div>
		</section>
	</div>
	<div class="row">
		<section>
			
			<div class="box">
				<h3>User</h3>
				<div class="userinfo" id="userinfo">
				</div>
			</div>
		</section>
	</div>
</div>