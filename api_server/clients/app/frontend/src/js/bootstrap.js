/**
 * @class bootstrap
 * @description Task runner for preparing the application environment,
 * executes a list of tasks as either sync or async.
 */
define('bootstrap', ['commands/setupApplicationCommand'], function(setupApplicationCommand) {
	'use strict';

	/**
	 * @private
	 * @type {array} Holds all tasks to be executed
	 */
	var _tasks = [];

	var bootstrap = {

		/**
		 * @function executeTask
		 * @description Called when the application index has loaded
		 */
		init: function() {
			this.executeTask();
		},

		/**
		 * @function executeTask
		 * @description Executes the first available task in an array of tasks
		 */
		executeTask: function() {
			if (_tasks.length > 0) {
				_tasks.shift()(bootstrap.executeTask);
			}
		},

		/**
		 * @function discardTasks
		 * @description Empties the task queue
		 */
		discardTasks: function() {
			_tasks = [];
		},

		/**
		 * @function getTaskList
		 * @description Gets the current tasks list
		 */
		getTaskList: function () {
			return _tasks;
		},

		/**
		 * @function addTask
		 * @description Adds a specified task to the back of a queue
		 * of tasks.
		 *
		 * @param {string} name of the command to execute
		 * @param {object} callback to trigger when a task is complete
		 */
		addTask: function(name, callback) {
			_tasks.push(callback);
		}
	};

	bootstrap.addTask('setupApplicationCommand', function (callback) {
		setupApplicationCommand.execute(bootstrap, callback);
	});

	return bootstrap;
});
