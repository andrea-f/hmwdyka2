/**
 * @class routes
 * @description Initialises app routing, making use of Rlite routing framework
 * https://github.com/chrisdavies/rlite
 */
define('framework/routes', function() {
	'use strict';

	return Rlite(); // jshint ignore:line
});
