/**
 * @class request
 * @description Initialises XHR requests, making use of reqwest micro framework
 * https://github.com/ded/reqwest
 *
 */
define('framework/request', ['libs/reqwest'], function(reqwest) {
	'use strict';
	return {
		get: reqwest
	};
});

