/**
 * @class request
 * @description Initialises XHR requests, making use of reqwest micro framework
 * https://github.com/ded/reqwest
 *
 */
define('framework/socket', ['libs/io'], function(io) {
	'use strict';
	return io;
});

