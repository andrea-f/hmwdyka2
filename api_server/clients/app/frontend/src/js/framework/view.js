/**
 * @class utils/view
 * @description Responsible for managing DOM updates. Can render templates and
 * cleanup afterwards. Templates can be rendered from the server, or passed as
 * template strings.
 */
define('framework/view', function() {
	"use strict";
	var container;
	return {
		/**
		 * @description Renders templates strings to the DOM. Cleans down any
		 * previous nodes before injecting new ones
		 * @param {object} options - containing configurations options:
		 * options.parent = parent DOM node to render the HTML into
		 * options.template = HTML template string to inject.
		 * options.callback = optional callback when a render is completed.
		 * options.append = append template instead of removing existing contents
		 */
		render: function (options) {

			var domNodes = '';

			if(options.parent && !document.getElementById(options.parent)) {
				throw new Error('`view.render()` cannot locate parent node in DOM');
			} else {
				container = (options.parent) ? document.getElementById(options.parent) : document.getElementById('view');
			}

			if(options.append) {
				domNodes = container.innerHTML;
			}

			if(options.template) {
				container.innerHTML = options.template + domNodes;
			}

			if(options.callback) {
				options.callback();
			}
		},

		/**
		 * @description Removes child HTML from DOM
		 * previous nodes before injecting new ones
		 * @param {string} parent - optional parent ID which child HTML
		 * needs to be removed from. if not passed the #view will be treated
		 * as the parent
		 */
		remove: function (parent) {

			if(parent && !document.getElementById(parent)) {
				throw new Error('`view.remove()` cannot locate parent node in DOM');
			} else {
				var container = document.getElementById(parent) || document.getElementById('view');
			}

			container.innerHTML = '';
		}
	};
});