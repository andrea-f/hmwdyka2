

/**
 * @class viewController
 * @description Manages controllers. Responsible for cleaning up when navigating to new controllers
 * This module behaves as a registry for controllers, implementing a standardised API for entry and
 * exit points.
 */
define('controllers/viewController', function() {
	'use strict';

	var viewController = {

		/**
		 * @type {object} accessor to the current controller available to the application
		 */
		_controller: null,

		/**
		 * @description: for a path requires the associated controller to
		 * setup the views and models etc.
		 *
		 * @param  {string} controllerName The controller to be associated each view
		 */
		init: function(controllerName, payload) {
			this._destroyCurrentController();
			if (controllerName) {
				require(['controllers/' + controllerName], function(newController) {
					this._controller = newController;
					this._controller.init(payload);
				}.bind(this));
			}
		},

		/**
		 * @description destroys any references to previous controllers before
		 * a new view controller is established
		 */
		_destroyCurrentController: function() {
			var view = document.getElementById('view');

			if (this._controller) {
				this._controller.destroy();
				view.innerHTML = '';
			}
		}
	};

	return viewController;
});
