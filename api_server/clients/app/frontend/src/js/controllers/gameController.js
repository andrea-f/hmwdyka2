

/**
 * @class gameController
 * @description The default screen showing showing how to interact with the
 * 
 */
define('controllers/gameController', [
    'text!templates/game.tpl',
    'framework/routes',
    'framework/view',
    'framework/request',
    'libs/io'
], function(
    game,
    routes,
    view,
    request,
    io
) {
    'use strict';

    var gameController = {

        /**
         * @type private key press listener
         */
        listener: null,
        items: {},
        fullscreen: false,
        films: [],
        game: {},
        player_loaded: false,
        player: {},
        client: {},
        withClient:0,
        clientLoaded: false,
        category: "",
        categories: {
            "film": "film",
            "videogames":"videogames",
            "football":"football",
            "cartoons": "cartoons",
            "music": "music"
        },


        /** 
         * @description Sets up on screen view and keylistener.
         */
        init: function(payload) {
            this.setupView();

            this.setupKeyListeners();
            //console.log(payload);
            if (this.categories[payload.params.category]) {
                    this.category = payload.params.category;       
                } else {
                    console.log("Category not found or given, defaulting to film.")
                    this.category = 'film';
                }
            if (typeof payload.params.withclient !== 'undefined' && parseInt(payload.params.withclient) === 1) {
                this.withClient = payload.params.withclient;
                var el = document.getElementById('playerrow');
                el.style.width = "70%";
                el.style.float = "left";
                var sp = document.getElementById('row-singleplayer');
                sp.style.width = "30%";
                sp.style.float = "right"
                sp.style.marginLeft = "0.2%";
                sp.style.marginRight = "0.2%";
                var B = document.body,
                    H = document.documentElement,
                    height;

                if (typeof document.height !== 'undefined') {
                    height = document.height // For webkit browsers
                } else {
                    height = Math.max( B.scrollHeight, B.offsetHeight,H.clientHeight, H.scrollHeight, H.offsetHeight );
                }
                console.log(height+"px")
                sp.style.height = height+"px";
                var spe = document.getElementById('singleplayer-embed')
                spe.style.height = height+"px";
                //this.showClient(document.getElementById('singleplayer'));
             } else {
                try {
                    console.log("Removing question element from DOM");
                    container.parentNode.removeChild(container);
                } catch (err) {
                    console.log(err);
                }
            }
            if (typeof payload.params.gamecode !== 'undefined' && payload.params.gamecode.length === 6) {
                this.client.gamecode = payload.params.gamecode;
                console.log("Attempting to join: "+ payload.params.gamecode);
                this.emitMessage('clientConnected', {
                    "gameCode":payload.params.gamecode,
                    "category":this.category
                });
            } else {
                var opts = {"category": this.category};
                if (typeof payload.params.key !== 'undefined' 
                    && typeof payload.params.item !== 'undefined') {
                    opts['key'] = payload.params.key;
                    opts['item'] = payload.params.item;
                }
                this.startGame(opts); 
            }
        },


        // try to join either from qs or form
        /**
         * @description signals when a new user joins a game.
         */
        joinGame: function(gameCode) {
            this.emitMessage("clientConnected",{"gameCode":gameCode});
        },


        /**
         * @description fills data on screen via UI.
         */
        fillData: function() {
            // TODO: User
            try {
                document.getElementById('creator').innerHTML="Game creator: "+this.game.users[0].facebook.name;
            } catch (err) {
                document.getElementById('creator').innerHTML = "Game creator: " + this.game.users[0];
            }
            document.getElementById('gameCode').innerHTML="Game code: "+this.game.gameCode;
            document.getElementById("totalquestions").innerHTML = "Asked questions: " + this.game.total_questions;
            var player_url = window.location.origin+"/#player?&gamecode="+this.game.gameCode;
            document.getElementById("connecttogame").innerHTML = "<a href="+player_url+" target='_blank'>Click here</a> to connect to game, or visit: <a href="+player_url+" target='_blank'>" + player_url + "</a>";
            // TODO Set up youtube url
        },


        welcome: function () {
            console.log("here");
        },


        /**
         * @description emits message to start game.
         * @param {object} data 
         */
        startGame: function(data) {
            this.emitMessage("startGame", data);
            console.log("Starting game...");
            //console.log(data);
        },


        /**
         * @description Setup listeners for RCU button presses, only
         * while in the context of the provisioning Controller screen
         */
        setupKeyListeners: function () {
            this.listener = this.listenToKeyPress.bind(this);
            window.addEventListener('onclick', this.listener);
        },


        // Probably want to separate the YouTube specific functions in new file?
        /**
         * @description loads YouTube player script
         * @param {string} id of video from YouTube. 
         */
        loadYouTubePlayer: function(id) {
            console.log("Loading YouTube player!");
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/iframe_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            this.loadYouTubePlayerReady(id)
        },


        /**
         * @description fires when YouTube player has loaded.
         */
        loadYouTubePlayerReady: function(id) {
            var player;
            var pc = this;
            window.onYouTubeIframeAPIReady = function() {
                player = new YT.Player('player', {
                    height: '100%',
                    width: '100%',
                    playerVars: {
                        'autoplay': 1,
                        'controls': 0,
                        'showinfo': 0,
                        'modestbranding': 1,
                        'autohide': 1
                    },
                    start: pc.game.video_start_time,
                    videoId: id,
                    modestbranding: 1,
                    events: {
                        'onReady': pc.onPlayerReady,
                        'onStateChange': pc.onYouTubePlayerChangeEvent
                    }
                });
            }
        },


        /**
         * @description fires when the player is ready.
         */
        onPlayerReady: function (event) {
            console.log("Player ready!");
            gameController.player = event.target;
            gameController.player.seekTo(gameController.game.video_start_time);
            gameController.player_loaded = true;
        },


        /**
         * @description Fires video has ended or changed. 
         */
        onYouTubePlayerChangeEvent: function (event) {
            //console.log("Change event: "+event.data)
            if (event.data == YT.PlayerState.ENDED) {
                //console.log(gameController.player)
                console.log("Video ended, asking for next question")
                gameController.emitMessage("nextQuestion", gameController.game)
            }
        },
        

        /**
         * @description Checks if interaction button press matches a designated action
         * @param {object} keydown event object
         */
        listenToKeyPress: function (event) {
            console.log(event);
        },


        /**
         * @description Render the initial screen for the app
         */
        setupView: function () {
            view.render({
                'template': game,
                'parent': 'view'
            });
        },


        /**
         * @description Removes event keys listener.
         */
        destroy: function() {
            window.removeEventListener('keydown', this.listener);
        },


        /**
         * @description Emits message wrapper.
         */
        emitMessage: function(method, data) {
            data.cookie = document.cookie;
            console.log("Sending cookie: "+document.cookie);
            console.log("socket game server", socket.id);
            socket.emit(method, data);
        },


        /**
         * @description fires when a client has joined.
         * @param {array} connected_clients.
         */
        clientJoined: function (connectedClients) {
            //this.game.connected_clients = connected_clients;
            console.log("Client joined");
            console.log(connectedClients);
            //if (connectedClients===undefined) {return}
            var c ="Connected clients:: ";
            if (typeof connectedClients !== undefined) {
                for (var p = 0; p < connectedClients.length; p++) {
                    c += " "+connectedClients[p].name+" - ";
                }
                document.getElementById('connectedclients').innerHTML=c;
            }
        },


        /**
         * @description Handles next question.
         */
        nextQuestion: function(game) {
            // GAME SET MATCH
            this.game = game;
            if (typeof this.client.gamecode !== 'undefined') {
                window.location.href = window.location.href.replace(this.client.gamecode, game.gameCode);
            } else {
                if (window.location.href.indexOf(game.gameCode) === -1) {
                    window.location.href += "&gamecode="+game.gameCode;
                };
            }
            var id = this.game.question.url.match(/(^|=|\/)([0-9A-Za-z_-]{11})(\/|&|$|\?|#)/)[2];
            console.log("Next question..."+id);
            if (this.player_loaded === false) {
                this.loadYouTubePlayer(id);
            } else {
                this.player.loadVideoById({
                    'videoId': id,
                    'startSeconds': this.game.video_start_time,
                    'suggestedQuality': 'large'
                })
            };
            //console.log("======SHOWING GAME======")
            //console.log(game);
            // Update connected clients:
            this.clientJoined(game.users)
            this.fillData();
            var container = document.getElementById('row-singleplayer');
            if (parseInt(this.withClient) === 1) { 
                this.container = document.getElementById('row-singleplayer');
                console.log("Showing questions on screen!");
                
                if (this.clientLoaded === false) {
                    this.container.parentNode.appendChild(this.container)
                    // new socket is created 
                    this.showClient(document.getElementById('singleplayer'));
                }
            } else {
                console.log("Removing question element from DOM");
                console.log(container)
                try {
                    container.parentNode.removeChild(container);
                } catch (err) {
                    console.log(err)
                }
            }

        },


        /**
         * @description Shows client controls.
         */
        showClient: function(container) {
            var questionsElement = document.createElement('iframe');
            questionsElement.id ='questions_client';
            // spawn a client
            questionsElement.width = '100%';
            questionsElement.height = '100%';
            //console.log(questionsElement)
            
            questionsElement.src = window.location.origin+"/#player?gamecode="+this.game.gameCode+"&withclient="+this.withclient;
            
            container.insertBefore(questionsElement, container.childNodes[1]);
            
            questionsElement.onload = (function(){
                var qe = this.contentDocument || this.contentWindow.document;
                qe.getElementById('app').style.overflowY = "hidden";
                console.log("Loaded questions and attached client.");
                gameController.clientLoaded = true;
            });
            console.log("In compact mode for "+this.game.gameCode+" !");
            this.clientLoaded === true;
        }
    };


    // Socket server url
    // PROD
    // var serverUrl = 'http://ec2-52-213-108-97.eu-west-1.compute.amazonaws.com:3000/',
    var serverUrl = 'http://localhost:3000/',
        socket = io.connect(serverUrl);

    socket.on("clientJoined", function (currentUsers){
        console.log("Client refresh!")
        console.log(currentUsers)
        gameController.clientJoined(currentUsers)
    })

    socket.on("sendToLogin", function (data){
        console.log("User is not logged in, redirecting to login page.");
        window.location = serverUrl + data.url;
    })

    socket.on("nextQuestion", function (game){
        console.log("Received a new question!!!123");
        console.log(game);
        console.log("socket id", socket.id)
        gameController.nextQuestion(game);
        gameController.clientJoined(game.users);
    })

    socket.on("gameNotFound", function (game){
        console.log("Game not found, asking to start a new game.");
        gameController.startGame(gameController.category);
    })

    socket.on("connectedToGame", function (game){
        console.log("Joined game: "+game.gameCode);
        gameController.nextQuestion(game);
        gameController.clientJoined(game.users);
    })    

    return gameController;
});

