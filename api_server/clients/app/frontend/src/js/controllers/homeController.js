

/**
 * @class homeController
 * @description The default screen showing showing how to interact with the
 * 
 */
define('controllers/homeController', [
    'text!templates/home.tpl',
    'framework/routes',
    'framework/view',
    'framework/request'
], function(
    home,
    routes,
    view,
    request
) {
    'use strict';
    var homeController = {
        init: function(payload) {
            console.log("In init of homeController!");
            this.setupView();
        },
        setupView: function () {
            view.render({
                'template': home,
                'parent': 'view'
            });
        },
    };
    return homeController;
});

