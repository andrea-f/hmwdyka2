

/**
 * @class playerController
 * @description The default screen showing showing how to interact with the
 * 
 */
define('controllers/playerController', [
    'text!templates/client.tpl',
    'framework/routes',
    'framework/view',
    'framework/request',
    'libs/io',
    'models/User'
], function(
    client,
    routes,
    view,
    request,
    io,
    User
) {
    'use strict';

    var playerController = {

        /**
         * @type private key press listener
         */
        listener: null,
        items: {},
        fullscreen: false,
        films: [],
        clicked: false,
        /** 
         * @description Sets up on screen view and keylistener.
         */
        init: function(payload) {
            this.setupView();
            this.setupKeyListeners();
            console.log("In player controller!");
            
            if (typeof payload.params.gamecode !== 'undefined' && payload.params.gamecode.length === 6) {
                console.log("Submitted gameCode: "+payload.params.gamecode)
                this.emitMessage('clientConnected', {"gameCode":payload.params.gamecode, "user": this.user})    
            }
        },


        /**
         * @description Adds question to questions element
         */
        fillQuestions: function(gameCode,q) {
            var questionsElement = questionsElement || document.getElementById('questions');
            questionsElement.innerHTML = "<h3>"+q.question+"</h3><br>";
            for (var s = q.answers.length-1; s >= 0 ; s--) {
                var answer = q.answers[s];
                var answerElement = this.createQuestionElement(answer, s)
                questionsElement.insertBefore(answerElement, questionsElement.childNodes[1]);
            }
        },


        /**
         * @description Creates a question button and adds an event listener.
         */
        createQuestionElement: function(answer, pos) {
            var answerElement = document.createElement('button');
            answerElement.id ='answer-'+pos;
            answerElement.className = "btn-block btn-lg btn btn-info"
            answerElement.innerHTML = answer;
            var p = this;
            //console.log("===USER====")
            //console.log(this)
            answerElement.addEventListener("click", function(event){
                event.preventDefault()
                p.submitAnswer(pos);
            });
            return answerElement;
        },

        /**
         * @description Submits an answer to the backend platform.
         */
        submitAnswer: function(answer) {
            // Remove links so another answer cannot be submitted
            // User can resubmit on refresh
            this.game.users[0].last_answer = answer;
            console.log("Submitted answer: "+answer+" first answer: "+(!this.clicked))
            if (this.clicked === true) {
                console.log("Answer already provided");
                //document.getElementById('answer_response').innerHTML = 'You already provided an answer to this question!';
                return;
            } else {
                var data = {};
                data.answer = answer;
                // Set user here
                data.user = this.game.users[0]//new User.User("andrea123"); // TODO: User
                // Send answer down the socket
                this.emitMessage("submittedAnswer", data);
            }
        },

        /** 
         * @description Populates user information on screen
         */
        fillUserInfo: function(user) {
            var userinfo = "<p class='text-center lead'>";
            
            userinfo += user.facebook.name;
            document.getElementById('userinfo').innerHTML=userinfo+"</p>";
        },

        /**
         * @description Setup listeners for RCU button presses, only
         * while in the context of the provisioning Controller screen
         */
        setupKeyListeners: function () {
            var report = this.createReport;
            this.listener = this.listenToKeyPress.bind(this);
            window.addEventListener('onclick', this.listener);
            var nextQuestionElement = nextQuestionElement || document.getElementById('nextquestion');
            var t = this;
            nextQuestionElement.addEventListener("click",function(event){
                //t.user.total_skipped += 1;
                // TODO: User
                //console.log("Skipped question, total skipped: "+ t.user.total_skipped)
                t.nextQuestion();
                event.preventDefault();
                
            });
            var newGameElement = newGameElement || document.getElementById('newgame');
            newGameElement.addEventListener("click",function(event){
                console.log("Asking for new game!");
                var url = "/#game?";
                if (typeof parent !== 'undefined') {
                    if (parent.location.hash.indexOf('withclient=1') !== -1) {
                        url += "&withclient=1";
                        parent.location.href = parent.location.origin+url;
                        parent.location.reload();
                    };
                } else {
                    console.log("Not in iframe....")

                }
               
                event.preventDefault();
            });
   
        },

        /**
         * @description Checks if RCU button press matches a designated action
         * @param {object} keydown event object
         */
        listenToKeyPress: function (event) {
            console.log(event)

        },

        /**
         * @description Render the initial screen for the app
         */
        setupView: function () {
            view.render({
                'template': client,
                'parent': 'view'
            });
        },

        /**
         * @description Removes event keys listener.
         */
        destroy: function() {
            window.removeEventListener('keydown', this.listener);
        },

        /**
         * @description Sends message to backend IO
         */
        emitMessage: function(method, data) {
            console.log("Emitting message: "+ method +" with data: ", data);
            data.cookie = document.cookie;
            console.log("Sending cookie: "+document.cookie);
            socket.emit(method, data);
        },

        /**
         * @description Signal when client has connected to game, then fills questions.
         */
        connectedToGame: function (game) {
            console.log("Client has connected to: " + game.gameCode);
            // TODO: Add list of users already participating in the game.
            this.fillQuestions(game.gameCode, game.question);
            this.game = game;
        },

        /**
         * @description updates UI when response has arrived if the user guess correctly or not.
         * @param {object} data
         */
        answerEvaluated: function (data) {
            //socket.on("evaluationresponse", function (data){
            this.clicked = false;
            // TODO: User
            this.fillUserInfo(data.user);
            this.user = data.user;
            console.log("Answer was correct: "+data.correct)
            //console.log(this.user)
            for (var v = 0; v < this.game.question.answers.length; v++) {
                document.getElementById('answer-'+v).disabled = "disabled";
            }
            var btn_type = ""
            data.correct ? btn_type="success": btn_type="danger";
            var colorate_answer = document.getElementById('answer-'+this.user.last_answer)
            colorate_answer.className = colorate_answer.className.replace('info', btn_type)

        },


        /**
         * @description Emits signal to go to next question.
         */
        nextQuestion: function() {
            this.emitMessage("nextQuestion",{"gameCode":this.game.gameCode});
        },

        /**
         * @description After a question is received , wait before putting it in screen.
         * @param {object} game 
         */
        receivedQuestion: function(game) {
            var pc = this,
                wait_time = 6;
            var qs = document.getElementById('questions');
            qs.innerHTML += "Next question in ";
            var loading = setInterval(function(){      
                qs.innerHTML+= ""+wait_time+"..."
                wait_time = (wait_time -1);
            }, 1000)
            setTimeout(function() { 
                clearTimeout(loading)
                this.button_type = "default";
                pc.fillQuestions(game.gameCode, game.question); 
            }, wait_time);
            
        }

    };

    // PROD
    // var socket = io.connect('http://ec2-52-213-108-97.eu-west-1.compute.amazonaws.com:3000/');
    var socket = io.connect('http://localhost:3000/');

    socket.on("welcome", function (data) {
        console.log("Connected to server!!");
        
    });
    socket.on("disconnected", function (data) {
        console.log("disconnected from server!!");
        
    });

    socket.on("nextQuestion", function (game) {
        console.log("Received a question!");
        playerController.receivedQuestion(game)
    });

    socket.on("answerEvaluated", function (data) {
        console.log("Received a question evaluation!");
        playerController.answerEvaluated(data)
    });

    socket.on("connectedToGame", function (game) {
        console.log("Connected to game!");
        socket.gameCode = game.gameCode;
        playerController.connectedToGame(game)
    });



    return playerController;
});

