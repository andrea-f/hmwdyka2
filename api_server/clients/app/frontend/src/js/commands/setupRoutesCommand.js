/**
 * @class setupRoutesCommand
 * @description sets up an application routing table for URL slashes, to controllers
 */
define('commands/setupRoutesCommand',
	['framework/routes', 'controllers/viewController'],
	function(routes, viewController) {
	'use strict';
	console.log("In setup route now")
	var setupRoutesCommand = {

		/**
		* @description: route mappings to controllers for the app
		*/
		ROUTES: [
			{ path: '#game', controller: 'gameController' },
			{ path: '#player', controller: 'playerController' },
			{ path: '#home', controller: 'homeController' }
			
		],

		execute: function(callback) {
			this.setupRoutes();

			if (callback) {
				callback();
			}
		},

		/**
		* @description: creates mapping of routes to respective controllers
		* via the viewContoller
		*/
		setupRoutes: function() {
			this.ROUTES.forEach(function(route) {
				routes.add(route.path, function(payload) {
					viewController.init(route.controller, payload);
				});
			});
		}
	};
		return setupRoutesCommand;
});
