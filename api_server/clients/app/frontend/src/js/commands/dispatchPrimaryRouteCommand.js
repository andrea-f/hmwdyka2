/**
 * Copyright 2016 YouView TV Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @class dispatchPrimaryRouteCommand
 * @description Removes the loading screen and displays the relevant starting
 * application screen
 */
define('commands/dispatchPrimaryRouteCommand', ['framework/routes'], function(routes) {
    'use strict';

    var dispatchPrimaryRouteCommand = {

        execute: function(callback) {
            console.log(location)
            this.setPrimaryRoute(location.hash);
            callback();
        },

        /**
         * @description works out the primary path for the app based on the URL
         * then triggers the apps routing mechanism
         *
         * @param {string} hash the URL bars hash params
         */
        setPrimaryRoute: function(hash) {
            
            console.log("hash : "+hash)
            routes.run(hash);
        }
    };

    return dispatchPrimaryRouteCommand;
});
