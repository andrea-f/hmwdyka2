/**
* @class setupApplicationCommand
* @description Executes tasks required to get the application into a ready state
*
*/
define('commands/setupApplicationCommand', [
		'bootstrap',
		'commands/setupRoutesCommand',
		'commands/dispatchPrimaryRouteCommand'
	],
	function(
		bootstrap,
		setupRoutesCommand,
       dispatchPrimaryRouteCommand) {
	'use strict';

	var setupApplicationCommand = {

		execute: function(bootstrap, callback) {
			this.addSetupTasks(bootstrap);

			if(callback) {
				callback();
			}
		},

		addSetupTasks: function(bootstrap) {

			bootstrap.addTask('setupRoutesCommand', function(callback) {
				setupRoutesCommand.execute(callback);
			});

			bootstrap.addTask('dispatchPrimaryRouteCommand', function(callback) {
				dispatchPrimaryRouteCommand.execute(callback);
			});
		}
	};

	return setupApplicationCommand;
});
