"use strict";

var pkg = require('../package.json'),
	gulp = require('gulp'),
	plugins = require('gulp-load-plugins')(),
	fs = require('fs'),
	del = require('del'),
	open = require('gulp-open'),
	requirejs = require('requirejs'),
	usemin = require('gulp-usemin'),
	minifyHtml = require('gulp-minify-html'),
	minifycss = require('gulp-minify-css'),
	replace = require('gulp-replace'),
	htmlreplace = require('gulp-html-replace'),
	
	uglify = plugins.uglify,
	sourcemaps = plugins.sourcemaps,
	concat = plugins.concat,
	header = plugins.header,
	rename = plugins.rename,
	uglifyConf = {
		compress: {
			drop_console: false
		},
		preserveComments: 'all'
	};

var BUILD_DIR = './dist';







/**
 * @description Creates a seperation of concerns for scripts.
 * JS is minified and compressed into relevent .min files to be
 * loaded in at runtime.
 */
gulp.task('amd', function() {
	requirejs.optimize({
						   findNestedDependencies: true,
						   preserveLicenseComments: false,
						   baseUrl: 'src/js',
						   optimize: 'uglify2',
						   name: 'libs/require',
						   inlineText: true,
						   //onBuildRead: function (moduleName, path, contents) {
							//   return contents.replace(/console.(log|debug|info)\((.*)\);?/g, '');
						   //},
						   'paths': {
							   text: 'libs/require-text'
						   },
						   include: [
							   'bootstrap',
							   'controllers/playerController',
							   'controllers/homeController',
							   'controllers/viewController',
							   'controllers/gameController',
						   ],
						   out: function (data) {
							   fs.readFile('./src/js/licence/licence.frag', 'utf8', function (err, license) {
								   var file = license + data;
								   fs.mkdir(BUILD_DIR, function (data) {
									   fs.mkdir(BUILD_DIR + '/js', function (data) {
										   fs.writeFile(BUILD_DIR + '/js/app.min.js', file, 'utf8', function (err) {
											   if (err) return console.log(err);
										   });
									   });
								   });
							   });
						   }
					   });
});

/**
 * @description Responsible for outputting minified HTML, CSS and JS.
 */
gulp.task('html', function() {
	fs.readFile('./.git/refs/heads/master', 'utf8', function (err, data) {
		var hash = data && data.replace(/ /g, '') || 'n/a';
		gulp.src('src/index.html')
			.pipe(htmlreplace({
								  require: {
									  src: [
										  ['js/app.min.js']
									  ],
									  tpl: '<script src="%s" data-main="js/init"></script>'
								  }
							  }))
			.pipe(htmlreplace({
								  init: {
									  src: [
										  ['js/init.js']
									  ],
									  tpl: '<script src="%s"></script>'
								  }
							  }))
			.pipe(usemin({
							 html: [minifyHtml({
												   empty: true
											   })],
							 js: [uglify(uglifyConf)],
							 css: [minifycss()]
						 }))
			
			.pipe(gulp.dest(BUILD_DIR+'/'));
	});
});


/**
 * @description Removes old builds, before creating anew
 */
gulp.task('clean', function() {
	del.sync(BUILD_DIR);
});

/**
 * @description The standard gulp process for building an app.
 * Runs through a list of tasks individually
 *
 * `$ gulp`
 */
gulp.task('default', ['clean'], function() {
	gulp.start('html', 'amd');
});