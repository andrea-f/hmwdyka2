## API server and frontend for How Much Do You Know About platform

# To get up and running with quiz and game creation service:
# Run: `npm install`
# Run: `npm start`
# Navigate to: `http://localhost:3000/quiz/`


# To get up and running with client:
# Cd into `clients`
# Run: `npm install`
# Run: 