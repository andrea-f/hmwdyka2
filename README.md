# README #

How much do you know about is a connected TV game using data from DBPedia and YouTube to create a quiz game about different categories. 
Data gathering is done via Python, backend and frontend is done via Node.js and Socket.io, jade used for templating. 
This is a Single Page Application with informative hash query, Database is Mongo (also nice with built in MapReduce).
Authentication is done via passport.js and Facebook.


# Screenshots #
An example filtering for Lazio football matches:
![select_football.png](https://bitbucket.org/repo/LAxnMM/images/190696126-select_football.png)
Different filters can be applied to the game parameters.
Then game is:
![play_football.png](https://bitbucket.org/repo/LAxnMM/images/2168206822-play_football.png)
Or videogames:
![select_videogames.png](https://bitbucket.org/repo/LAxnMM/images/234386638-select_videogames.png)
Then the game:
![play_videogames copy.png](https://bitbucket.org/repo/LAxnMM/images/3418290958-play_videogames%20copy.png)


# TODO #

* Deploy to AWS so it is viewable
* Recreate fb app to allow for user to log in via fb
* Define scores victories etc