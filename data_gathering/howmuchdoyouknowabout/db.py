from pymongo import MongoClient, ASCENDING
from pprint import pprint
import os, sys

class DB(object):
	def __init__(self, category):
		"""Creates client connection"""
		try:
			self.client = MongoClient('localhost', 27017)
			db = self.client.items
			self.collection_category = db[category]
			res = self.createIndex()
		except:
			self.client = MongoClient('db', 27017)
			db = self.client.items
			self.collection_category = db[category]
			res = self.createIndex()
		
		if res is False:
			print "Index on %s already created." % category


	def read(self, key, value):
		"""Reads an object from the database"""
		try:
			results = self.collection_category.find({key: value})
			return {
				"results": results,
				"total": results.count()
			}
		except Exception as e:
			print "[read] Error: %s " % e 

	def createIndex(self):
		"""Creates an index on category collection"""
		if len(list(self.collection_category.index_information())) == 0:
			result = self.collection_category.create_index([('url', ASCENDING)], unique=True)
			return result
		else:
			return False

	def save(self, obj):
		"""Saves an object to the database"""
		#pprint(obj)
		try:
			video_exists = self.collection_category.find_one({"url": obj['url']}).count()
		except:
			video_exists = 0
		print "[save] Video exists: %s" % video_exists
		if video_exists == 0:
			try:
				video_id = self.collection_category.insert_one(obj).inserted_id
				return video_id
			except Exception as e:
				print "[save] Error in saving: %s" % e
				return False
		else:
			return False

	def updated(self, obj):
		"""Updates an object"""

	def delete(self, obj):
		"""Deletes an object from db"""

	def exists(self, url):
		exists = self.collection_category.findOne({"url":url})
		print exists
		sys.exit(2)
		if exists is None:
			return True
		return False

if __name__ == "__main__":
	hm = DB(category)
    

    


