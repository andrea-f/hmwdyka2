# README #

# Python Scraper for matching DBPedia SPARQL query with YouTube. #
## 1. - Config options are in HMDYKA_CONFIG ##
## 2. - cd in api ##
## 3. - run: python DBPedia.py ##
## 4. - then cd out and run python howmuchdoyouknowabout.py ##


## To tune options for DBPedia, use:
##�python DBPedia.py "../DBPEDIA_QUERY" "videogames" "" 
## For Italian DBPedia
## To download the data: python DBPedia.py "../DBPEDIA_QUERY" "film" "it"
## To search youtube: python howmuchdoyouknowabout.py "football" "_football.json"
## Current categories are: film, football, cartoons, videogames, history (politics)


