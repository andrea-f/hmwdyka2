# -*- coding: utf-8 -*-
import sys, os
from Film import Film

#"value",
#"title",
#"start_date",
#"author",
#"country",
#"gender",
#"date"
class Cartoons(Film):
    """This class creates an object containing a url query score and items. It is derived from Film"""

    def __init__(self, config = {}):
        """ Initializes class specific properties"""
        self.config = config


    #@classmethod
    def convertDictToQuery(self, items = [], helperWord = ""):
        """Converts JSON to list of queries."""
        ### check recursively
        queries = []
        hasDate = False
        for item in items:
            q = "%s" % (item['title'])
            try:
                if not item['date'] in q:
                    q += " %s" % item['date'][:4]
                    hasDate = True
            except:
                try:
                    q += " %s" % item['start_date']
                    item['date'] = item['start_date']
                    hasDate = True
                except:
                    pass
            if len([w for w in item['title'].split(' ') if len(w)>=2]) < 3:
                q += " %s" % helperWord
            if hasDate is True:
                queries.append((q,item, self.config["CATEGORY"], self.config["ROOT"]))
        return queries


    #@classmethod
    def scoreVideo(self, video):
                # Check if video item is in description
        score = video.score

        return score

    def filterForValues(self, json_page):
        json_filtered_page = json_page['results']['bindings']
        items = []


        for item in json_filtered_page:
            jsf = {}

            attori = item['attori']['value'].replace('"','').replace('@it','').split('*')
            jsf['attori'] = []
            for attore  in attori:
                a = {}
                nome = attore.split(':')[0].lstrip().replace('\n','')
                if len(nome) > 0:
                    a['nome'] = nome

                    try:
                        interpreta = attore.split(':')[1].lstrip().replace('\n','')
                        if len(interpreta) > 0:
                            a['interpreta'] = interpreta
                    except:
                        pass
                    jsf['attori'].append(a)
            for key, val in item.iteritems():
                #for subkey, subval in val.iteritems():
                #    jsf[key] = 
                if 'attori' not in key:
                    jsf[key] = val['value'].split('/')[-1].replace('_', ' ')
                    try:
                        jsf[key] = jsf[key].replace(' (Cartoons)','')
                    except:
                        pass


                    try:
                        jsf[key] = jsf[key].replace(' (Cartoons %s)'%item['data']['value'],'')
                    except:
                        pass

                    try:
                        if "title" in key:
                            jsf[key] = jsf[key].replace(item['data']['value'],'')
                    except:
                        pass
            items.append(jsf)
        return items