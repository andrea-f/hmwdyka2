# -*- coding: utf-8 -*-
import sys, os
class Film(object):
    """This class creates an object containing a url query score and items."""

    def __init__(self, config = {}):
        """ Initializes class specific properties"""
        self.config = config
        self.plurals = ["producers", "paesi", "writers"]


    def mapKeysData(self):
        """Maps retrieved keys to set values so scoring can be performed in a scalable and tunable way."""
        return {
            "title":"title",
            "date":"data",
            "top_names":"starring", # for videogames it could be developer/s
            "genres":"genere",
            "characters": "attori", # what about name and interpreta?
            "locations": "paese"
        } 

    #@classmethod
    def convertDictToQuery(self, items = [], helperWord = "film"):
        """Converts JSON to list of queries."""
        ### check recursively
        queries = []
        for item in items:
            q = "%s" % (item['title'])
            if not item['date'] in q:
                q += " %s" % item['date']
            if len([w for w in item['title'].split(' ') if len(w)>=2]) < 3:
                q += " %s" % helperWord
            queries.append((q,item, self.config["CATEGORY"], self.config["ROOT"]))
        return queries

    def checkItemsInVideo(self,item_val,video):
        score = 0
        if isinstance(item_val, dict):
            for key, val in item_val.iteritems():
                if "nome" in key:
                    item_val = val
                    break 

        try:
            if item_val.lower() in video.description.lower():
                score += int(self.config["ITEM_IN_DESCRIPTION"])
            if item_val.lower() in [t.lower() for t in video.tags]:
                score += int(self.config["ITEM_IN_TAGS"])
        except Exception as e:
            print "[checkItemsInVideo] FILM Error: %s" % e
            pass
        return score

    def scoreVideo(self, video):
                # Check if video item is in description
        score = video.score
        for item_key, item_val in video.item.iteritems():
            if 'attori' in item_key:
                # attore
                for attore in video.item['attori']:
                    if attore['nome'].lower() in video.description.lower():
                        score += int(self.config["ITEM_IN_DESCRIPTION"])

                    if attore['nome'].lower() in [t.lower() for t in video.tags]:
                        score += int(self.config["ITEM_IN_TAGS"])
            try:
                if item_key in self.plurals:
                    for item in video.item[item_key]:
                        try:
                            if item.lower() in video.title.lower():
                                score += int(self.config["PLURAL_IN_TITLE"])
                        except:
                            pass
                        try:
                            if item.lower() in video.description.lower():
                                score += int(self.config["PLURAL_IN_DESCRIPTION"])
                        except:
                            pass
                        try:
                            if item.lower() in [t.lower() for t in video.tags]:
                                score += int(self.config["PLURAL_IN_TAGS"])
                        except Exception as e:
                            pass
            except Exception as e:
                print "[Film.scoreVideo] Error in checking plurals: %s"%e
        return score

    def filterForValues(self, json_page):
        json_filtered_page = json_page['results']['bindings']
        items = []
        for item in json_filtered_page:
            jsf = {}

            attori = item['attori']['value'].replace('"','').replace('@it','').split('*')
            jsf['attori'] = []
            for attore  in attori:
                a = {}
                nome = attore.split(':')[0].lstrip().replace('\n','')
                if len(nome) > 0:
                    a['nome'] = nome
                    try:
                        interpreta = attore.split(':')[1].lstrip().replace('\n','')
                        if len(interpreta) > 0:
                            a['interpreta'] = interpreta
                    except:
                        pass
                    jsf['attori'].append(a)
            for plural in self.plurals:
                jsf[plural] = []
                for sub_item in item[plural]['value'].split(';;;'):
                    try:
                        sub_item = sub_item.lstrip().replace('\n','').split('/')[-1].replace('_', ' ')
                    except:
                        pass
                    if len(sub_item) > 0 and not sub_item in jsf[plural]:                    
                        jsf[plural].append(sub_item)
            for key, val in item.iteritems():
                if not 'attori' in key and not key in self.plurals:
                    jsf[key] = val['value'].split('/')[-1].replace('_', ' ')
                    try:
                        jsf[key] = jsf[key].replace(' (film)','')
                    except:
                        pass
                    try:
                        jsf[key] = jsf[key].replace(' (film %s)'%item['data']['value'],'')
                    except:
                        pass

                    try:
                        if "title" in key:
                            jsf[key] = jsf[key].replace(item['data']['value'],'')
                    except:
                        pass

            items.append(jsf)
        return items