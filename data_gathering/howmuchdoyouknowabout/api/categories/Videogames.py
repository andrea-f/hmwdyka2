# -*- coding: utf-8 -*-
import sys, os
class Videogames(object):
    """This class creates an object containing a url query score and items."""

    def __init__(self, config = {}):
        """ Initializes class specific properties"""
        self.config = config
        self.plurals = ["platforms","genres"]

#select DISTINCT ?title ?data ?developer 
# (group_concat(distinct ?platform;separator=":") as ?platforms) 
# (group_concat(distinct ?genre;separator=":") as ?genres) where { 
# title, date, developer, platforms, genres
    

    def convertDictToQuery(self, items = [], helperWord = "videogame"):
        """Converts JSON to list of queries."""
        ### check recursively
        queries = []
        for item in items:
            q = "%s" % (item['title'])
            if not item['date'] in q:
                q += " %s" % item['date']
            if len([w for w in item['title'].split(' ') if len(w)>=2]) < 3:
                q += " %s" % helperWord
            queries.append((q,item, self.config["CATEGORY"], self.config["ROOT"]))
        return queries



    def scoreVideo(self, video):
                # Check if video item is in description
        score = video.score
        for item_key, item_val in video.item.iteritems():
            for plural in self.plurals:
                if plural in item_key:

                    for sub_item in video.item[plural]:
                        #for sub_item in sub_items:
                            print "subitem: %s"%sub_item 
                            print "title: %s"%video.title
                            print "wikititle: %s"%video.item['title']
                            if sub_item.lower() in video.description.lower():
                                score += int(self.config["ITEM_IN_DESCRIPTION"])

                            if sub_item.lower() in [t.lower() for t in video.tags]:
                                score += int(self.config["ITEM_IN_TAGS"])
        return score

    def filterForValues(self, json_page):
        json_filtered_page = json_page['results']['bindings']
        items = []
        for item in json_filtered_page:
            jsf = {}
            for plural in self.plurals:
                #.replace('"','').replace('@it','').split('*')
                jsf[plural] = []
                for sub_item in item[plural]['value'].split(';;;'):
                    sub_item = sub_item.lstrip().replace('\n','').split('/')[-1].replace('_', ' ')
                    if len(sub_item) > 0 and not sub_item in jsf[plural]:                    
                        jsf[plural].append(sub_item)


            for key, val in item.iteritems():
                #for subkey, subval in val.iteritems():
                #    jsf[key] = 
                if not 'genres' in key and not 'platforms' in key:
                    jsf[key] = val['value'].split('/')[-1].replace('_', ' ')
                    try:
                        jsf[key] = jsf[key].replace(' (Video Game)','')
                    except:
                        pass
                    try:
                        jsf[key] = jsf[key].replace(' (film %s)'%item['date']['value'],'')
                    except:
                        pass

                    try:
                        if "date" in key:
                            jsf[key] = jsf[key][:4]
                    except:
                        pass

                    try:
                        if "title" in key:
                            jsf[key] = jsf[key].replace(item['date']['value'],'')
                    except:
                        pass
            items.append(jsf)
        return items

