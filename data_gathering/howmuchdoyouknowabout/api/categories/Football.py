# -*- coding: utf-8 -*-
#!/usr/bin/env python
import json
import requests
from pprint import pprint as pp2
import sys, os
import urllib
import traceback
import re
sys.setrecursionlimit(2000)
sys.path.append('/usr/local/lib/python2.7/site-packages')
from unidecode import unidecode
class Football(object):
    """This class creates an object containing a url query score and items."""

    def __init__(self, config = {}):
        """ Initializes class specific properties"""
        self.config = config
        self.plurals = ["presidents", "coaches"]
        self.remove_words = [
            "libertas",
            "bergamasca",
            "Polisportiva",
            "Foot-Ball",
            "Football",
            "A.C.",
            "S.P.A.",
            "U.C.",
            "A.S.",
            "F.C.",
            "U.S.",
            "Club",
            "Sezione",
            "Calcio",
            "foot ball",
            "1893 Circolo del",
            "Unione",
            "Sportiva",
            "Associazione",
            "Giuoco del",
            "Società",
            "Cricket And",
            "Città",
            "Sporting",
            "Podistica",
            "S.S.",
            "Associazioni",
            "Riunite",
            "And Cricket",
            "Sport",
            "Et Libertate",
            "Ginnastica",
            "di ",
            "Http://It.Dbpedia.Org/Resource/",
            "__",
            "Fascista",
            "Del In ",
            "Civile Campo ",
            "Acf",
            "1893",
            "1898",
            "Cisitalia",
            "_",
            "-"

        ]

        self.abbreviations = {
            "Internazionale Milano":"Inter",
            "Ambrosiana-Inter": "Inter",
            "Internazionale-Naples":"Inter",
            #"Internazionale":"Inter",
            "Inter Milano": "Inter"
        }


    ##### FOR SCORING
    

    def convertDictToQuery(self, items = [], helperWord = "calcio"):
        """Converts JSON to list of queries."""
        queries = []
        # Need to create association between teams so they appear one against the other
        self.items = items

        total_items = len(items)-1
        #print total_items
        c = 0
        self.new_items = []

        while c  <= total_items:
            ret = self.createMatch(items[c], total_items)
            c += 1
        print "Generated %s items with matches..." % len(self.new_items)
        for item in self.new_items:
            for match in item['matches']:
                #print match
                q = "%s" % (match)
                if not item['date'] in q:
                    q += " %s" % item['date']
                query = (q,item, self.config["CATEGORY"], self.config["ROOT"])
                #print query
                queries.append(query)
        #pp2(queries)
        return queries

    def createMatch(self, item, total_items):
        """Recursively creates match per year for all teams"""
        try:
            isinstance(item['matches'], list)
        except Exception as e:
            #print "[createMatch] is not a list: %s" % e
            item['matches'] = []
        try:
            if item['date'] in self.items[total_items-1]['date'] and not (item['club'] in self.items[total_items-1]['club']):
                match = "%s %s" % (item['club'], self.items[total_items-1]['club'])
                item['matches'].append(match)
        except Exception as e:
            print "[createMatch] Error : %s" % e
            pp2(self.items[total_items-1])
            sys.exit(2)

        if (total_items == 0):
            #print "Returning item:"
            self.new_items.append(item)
            return True
        else:
            if not( total_items-1 ) is None:
                self.createMatch(item, total_items - 1)
            else:
                return False

    def hasRequired(self, item, feed, q):
        """Check if it has required key"""
        ids = [] 
        s = q.split()[:-1]
        home = item['club']
        away = ' '.join(s).replace(home, '').strip()
        for entry in feed:
            if (home.lower() in entry['snippet']['title'].lower() and away.lower() in entry['snippet']['title'].lower() and item['date'] in entry['snippet']['title']) or (home.lower() in entry['snippet']['description'].lower() and away.lower() in entry['snippet']['description'].lower() and item['date'] in entry['snippet']['description'] ):
                try:
                    ids.append(entry['id']['videoId'])
                except:
                    pass   
        return ids  


    def scoreVideo(self, video):
        """ Per category video scoring """
        s = video.query.split()[:-1]
        home = video.item['club']
        away = ' '.join(s).replace(home, '').strip()
        print home, away
        index_home_title = video.title.lower().find(home.lower())
        index_away_title = video.title.lower().find(away.lower())
        index_home_description = video.description.lower().find(home.lower())
        index_away_description = video.description.lower().find(away.lower())
        if (index_home_title != -1 and index_away_title != -1):
            if index_away_title > index_home_title:
                video.score += int(self.config["SCORE_GAME_HOMEAWAY_TITLE"])
            else:
                video.score -= int(self.config["SCORE_GAME_HOMEAWAY_TITLE"])
        if (index_home_description != -1 and index_away_description != -1):
            if index_away_description > index_home_description:
                video.score += int(self.config["SCORE_GAME_HOMEAWAY_DESCRIPTION"])
        return video.score

    def checkTitleAndDescriptionWordLength(self, score, video):
        return 0   

    def checkItemsInVideo(self,item_val,video):
            score = 0
            if not item_val.isdigit():
                try:
                    if item_val.lower() in video.description.lower():

                        score += int(self.config["ITEM_IN_DESCRIPTION"])

                    if item_val.lower() in video.title.lower():
                        score += int(self.config["MOVIE_IN_TITLE"])

                    if item_val.lower() in [t.lower() for t in video.tags]:
                        score += int(self.config["ITEM_IN_TAGS"])
                except Exception as e:
                    print "[checkItemsInVideo] Error: %s" % e
                    pass
            return score


    ###### FOR DBPEDIA!



    def resolveDBPediaQuery(self, q, f='application/json', format="json", query_file_location = ""):
        """

        :param username:
            Reddit registered user name, string.

        :param password:
            Password associated with user name, string.

        Return requests session client with Reddit login info after successfull login.
            
        """
        print '{DBPedia} [resolveDBPediaQuery] begin article fetching\n'

        host = "http://uriburner.com/sparql"

        # need to fetch multiple...by offset

        try:
            query = urllib.quote(str(open(query_file_location).read()))

        except Exception as e:
            print "Error in encoding DBpedia URL: %s" % e
            pass

        try:
            #POST with user/pwd
            client = requests.session()
            url = r'{host}/?timeout=30000000&default-graph-uri=&query={query}&format={format}'.format(host=host, query=query,format=format)
            try:
                r = client.get(url)
                print 'Requested URL is', r.url
                resp = json.loads(r.text)

            except Exception as e:
                print "Reading from file because %s" % e
                f = open('data/_football_RAW.json','r')
                resp = json.loads(f.read())
                f.close()
            return resp

        except Exception as e:
            print "{DBPedia} [resolveDBPediaQuery] error %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "{DBPedia} [resolveDBPediaQuery] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)

            print error
            print r
            pass
            sys.exit(2)
            return ""


    def cleanName(self, club):
        """
        Cleans a name up to improve searching later on.
        """
        w = str(club.encode('utf-8')).lower()
        for word in self.remove_words:
            try:
                v = str(word.encode('utf-8')).lower()
            except:
                v = str(word).lower()
            if v in w:
                w = w.replace(v, '')
        c = w.title().strip().replace("  ", " ")
        search_dates = r"(?:(?:19|20)[0-9]{2})"
        dates_in_club = re.findall(search_dates, c)
        for dt in dates_in_club:
            c = c.replace(dt, '')
        if len(c) > 0:
            for abbr, repl in self.abbreviations.iteritems():
                if abbr.lower() in c.lower():
                    c = c.replace(abbr, repl)
                            # can be optimized
            c = c.replace("Internazionale", "Inter").replace("Del In ", '')
            return c

    def filterForValues(self, json_page):
        """
        Cleans up data retrieved from DBPedia.
        """
        json_filtered_page = json_page['results']['bindings']
        items = []
        for item in json_filtered_page:
            jsf = {}
            for plural in self.plurals:
                jsf[plural] = []
                for sub_item in item[plural]['value'].split(';;;'):
                    try:
                        sub_item = sub_item.lstrip().replace('\n','').split('/')[-1].replace('_', ' ')
                    except:
                        pass
                    if len(sub_item) > 0 and not sub_item in jsf[plural]:                    
                        jsf[plural].append(sub_item)
            for key, val in item.iteritems():
                if key not in self.plurals:
                    if "club" in key:
                        club = val['value']
                        c = self.cleanName(club)
                        # If for some reason the club name is empty or too short.
                        if not c:
                            c = self.cleanName(item['team']['value'])
                        jsf[key] = c
                    else:
                        jsf[key] = val['value'].split('/')[-1].replace('_', ' ')
                    try:
                        if "date" in key:
                            jsf[key] = jsf[key][:4]
                    except:
                        pass

                    try:
                        if "title" in key:
                            jsf[key] = jsf[key].replace(item['data']['value'],'')
                    except:
                        pass
            items.append(jsf)

        # Set items as class variable so can be accessed later
        self.items = items
        return items




