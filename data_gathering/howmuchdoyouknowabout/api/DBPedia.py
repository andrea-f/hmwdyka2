#!/usr/bin/env python
import sys
import json
import copy
try:
    sys.path.append("/usr/local/lib/python2.7/site-packages")
except:
    pass
import requests
from pprint import pprint as pp2
import os
import urllib
import traceback
import importlib
CATEGORIES = {
    "film": "Film",
    "videogames":"Videogames",
    "football": "Football",
    "cartoons": "Cartoons",
    #"politics": "Politics"
}

class DBPedia(object):

    def __init__(self, query_file_location = "../DBPEDIA_QUERY", language = "", category = "film"):
        """"""
        self.query_file_location = query_file_location
        self.language = language
        self.category = category
        try:
            if CATEGORIES[category]:
                module = importlib.import_module('categories.'+CATEGORIES[category])
                my_class = getattr(module, CATEGORIES[category])
                print "Getting items by: %s" % category
                self.categoryClass = my_class()
        except:
            print "[DBPedia] No matching category for: %s" % category




    def resolveDBPediaQuery(self, q, f='application/json', format="json", query_file_location = None):
        """Retrieves data from DBPedia        
        """
        print '{DBPedia} [resolveDBPediaQuery] begin article fetching from %s\n' % query_file_location

        try:
            query = urllib.quote(str(open(query_file_location).read()))
        except Exception as e:
            print "Error in opening DBPedia query file: %s" % e
            try:
                query = urllib.quote(str(q))
            except:
                query = q

        host = 'http://uriburner.com/sparql'
        try:
            client = requests.session()
            url = r'{host}/?timeout=30000000&default-graph-uri=&query={query}&format={format}'.format(host=host, query=query,format=format)
            #pp2(url)
            r = client.get(url)
            return json.loads(r.text)
        except Exception as e:
            print "{DBPedia} [resolveDBPediaQuery] DBPedia retrieval error error %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = "{DBPedia} [resolveDBPediaQuery] Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            return ""

    def _writeFile(self, json_file, data, overwrite = True, save_key = ""):
        fn = json_file+".json"
        if not overwrite:
            try:
                f = open(fn, 'r')
                dt = json.load(f)
                f.close()
                if not len(save_key) == 0:
                    try:
                        dt[save_key] = dt[save_key] + data
                    except:
                         dt[save_key] = data
                    data = copy.deepcopy(dt)
                else:
                    try:
                        data['results']['bindings'] = dt['results']['bindings'] + data['results']['bindings']
                    except:
                        data = data+dt
                f = open(fn, "w+")
            except Exception as e:
                f = open(fn, 'w')
                print "[DBPedia][_writeFile] Error in saving: %s" % e
                pass
        else:
            f = open(fn, 'w')
        f.write(str(json.dumps(data, indent=4, sort_keys=True)))
        print "Wrote %s - %s" % (fn, os.path.isfile(json_file))
        return True
            
    def runDBPediaQuery(self):
        """
        Fetches data using SPARQL from DBPedia
        """
        # Query to send
        file = open(self.query_file_location, 'r')
        query = file.read()
        try:
            json_page = self.categoryClass.resolveDBPediaQuery(query, query_file_location = self.query_file_location)
        except:
            json_page = self.resolveDBPediaQuery(query, query_file_location = self.query_file_location)
        self.processPage(json_page)

    def processPage(self, json_page, json_file = '../data/', category = "", overwrite = True, save_key = ""):
        if len(json_page) > 0:
            try:
                json_filtered_page = self.categoryClass.filterForValues(json_page)
            except:
                json_filtered_page = self.filterForValues(json_page)
            if len(category) == 0:
                category = self.category
            fn = json_file+"_"+category
            self._writeFile(fn+"_RAW", json_page, overwrite = overwrite)
            self._writeFile(fn, json_filtered_page, overwrite = overwrite)
        else:
            print "Nothing to filter out for...."

    def filterForValues(self, json_page, separator = ";;;"):
        """
        Standard json creation
        """
        json_filtered_page = json_page['results']['bindings']
        items = []
        for item in json_filtered_page:
            jsf = {}
            # Treat all entries if it was a single key, like not a list or a dict
            for key, val in item.iteritems(): 
                if separator in item[key]['value']: 
                    jsf[key] = []
                    for sub_item in item[key]['value'].split(separator):
                        try:
                            sub_item = sub_item.lstrip().replace('\n','').split('/')[-1].replace('_', ' ')
                        except:
                            pass
                        if len(sub_item) > 0 and not sub_item in jsf[key]:                    
                            jsf[key].append(sub_item)
                else:
                    jsf[key] = val['value'].split('/')[-1].replace('_', ' ')
                    try:
                        jsf[key] = jsf[key].replace(' (%s)'+self.category,'')
                    except:
                        pass
                    try:
                        jsf[key] = jsf[key].replace(' (%s %s)'%(self.category,item['date']['value']),'')
                    except:
                        pass
                    try:
                        if "title" in key:
                            jsf[key] = jsf[key].replace(item['date']['value'],'')
                    except:
                        pass
                    try:
                        if "date" in key:
                            jsf[key] = jsf[key][:4]
                    except:
                        pass
            items.append(jsf)
        return items

def main():
    try:
        query_file_location = sys.argv[1]
    except:
        query_file_location = "../DBPEDIA_QUERY"

    try:
        category = sys.argv[2]
    except:
        category = "film"

    try:
        language = sys.argv[3]
    except:
        language = ""
    try:
        dbp = DBPedia(language=language, query_file_location = query_file_location, category = category)
    except Exception as e :
        print "Error in values: %s" % e
        sys.exit(2)
        #dbp = DBPedia(language="it")
    
    saved = dbp.runDBPediaQuery()

    # python DBPedia.py "../DBPEDIA_QUERY" "" "videogames"

if __name__ == "__main__":

    main()