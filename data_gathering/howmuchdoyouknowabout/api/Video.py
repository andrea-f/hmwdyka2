class Video(object):
    """This class creates an object containing a url query score and items."""

    def __init__(self,
        
        uploader = None,
        url = None,
        query = None,
        score = None,
        items = [],
        date = None,
        category = None,
        title = None,
        duration = None,
        image = None,
        description = None,
        viewcount = None,
        uploadDate = None,
        root = None,
        **videohash):
        self.url = url
        self.query = query
        self.score = score
        self.items = items
        self.date = date
        self.category = category
        self.description = description
        self.title = title
        self.duration = duration
        self.image = image
        self.viewcount = viewcount
        self.root = root
        self.uploader = uploader


