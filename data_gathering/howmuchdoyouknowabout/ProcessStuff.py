import os, sys
from api.DBPedia import DBPedia
import datetime 
import time
import re
import copy
import json


class ProcessStuff(DBPedia):
    """This class downloads stuff from DBPedia."""
    
    def __init__(self, fn = "" ):
        """Initializes ProcessStuff class"""
        self.filePath = fn

    def getConfigFile(self):
        """Reads json file with dbpedia query information"""
        try:
            jsonFile = open(self.filePath, "r")
            data = json.load(jsonFile)
            jsonFile.close()
            return data
        except Exception as e:
            print "[getConfigFile] Error in reading file: %s, quitting." % e
            sys.exit(2)

    def queryMultiplier(self, data, identifier = '[[X]]'):
        """Generate new DBPedia queries based on previous query result."""
        queries = []
        q = self.createSparqlQuery(data)
        json_page = self.resolveDBPediaQuery(q = q)
        file_name = data['category']+"___"+data['sub_category']
        self.processPage(json_page, json_file = 'data/', category = file_name, overwrite = True)
        
        if len(data['sub_page']) > 0:
            try:
                save_key = [var for var in data['select'] if var.endswith('_S')][0].replace('_S','')
            except:
                save_key = "sub_page"
            try:
                items = json_page['results']['bindings']
                for item in items:
                    sub_data = copy.deepcopy(data)
                    # Only allows for one identifier
                    sub_page_identifier = [var for var in data['select'] if '_X' in var][0].replace('_X','')
                    name = item[sub_page_identifier]['value']
                    count = 0
                    while count < len(sub_data['sub_page']):
                        if identifier in sub_data['sub_page'][count]:
                            sub_data['sub_page'][count] = sub_data['sub_page'][count].replace(identifier,name)
                        count += 1
                    q = self.createSparqlQuery(sub_data, key = 'sub_page')
                    queries.append(q)
            except Exception as e:
                print "[ProcessStuff][queryMultiplier] Error in creating queries for subpages: %s" % e
                pass
            for query in queries:
                file_name = data['category']+"___"+data['sub_category']
                print "Fetching query: \n%s" % query
                json_page = self.resolveDBPediaQuery(q = query)
                print "Processing page and saving to: "+ file_name
                self.processPage(json_page, json_file = 'data/', category = file_name, overwrite = False, save_key = save_key)


    def createConcat(self, data, separator = ";;;"):
        """ Creates concat string. """
        return "(group_concat(distinct ?"+data+";separator='"+separator+"') as ?"+data+"_s)" 

    def createSparqlQuery(self, data, separator = ";;;", key = "root", offset = 100):
        """Generates SPARQL query from input file."""
        query = []
        orderby = []
        select = "SELECT DISTINCT"
        #from_each_subpage
        for prop in data['select']:
            if prop.endswith("_s"):
                select +=" "+ self.createConcat(prop.split("_")[0])
            else:
                v = "?"+ prop.replace('_X','')
                select += " "+ v
                orderby.append(v)
        where = " WHERE { "
        closing = 1
        query.append(select)
        query.append(where)
        try:
            service = "SERVICE "+data['service'] + " {"
            query.append(service)
            closing += 1
        except:
            pass
        query.append('\n'.join(data[key]))
        while closing > 0:
            query.append('}')
            closing -= 1
        o = " ORDER BY " + ' '.join(orderby)
        query.append(o)
        try:
            limit = data['limit']
            l = " LIMIT %s" % limit
            query.append(l)
        except:
            pass

        complete_query = '\n'.join(query)
        print complete_query
        return complete_query

if __name__ == "__main__":
    
    try:
        JSON_FILE_NAME = sys.argv[1]
    except:
        print "JSON file name is needed to run!"
        sys.exit(2)
    start_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    hm = ProcessStuff(JSON_FILE_NAME)
    data = hm.getConfigFile()
    print data
    hm.queryMultiplier(data[0])
    end_time = datetime.datetime.now().time().strftime('%H:%M:%S')
    total_time=(datetime.datetime.strptime(end_time,'%H:%M:%S') - datetime.datetime.strptime(start_time,'%H:%M:%S'))
    print "Took %s to process %s " % (total_time, JSON_FILE_NAME)

    

