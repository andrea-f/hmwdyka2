# To change this template, choose Tools | Templates
# and open the template in the editor.

#Code provided by www.dinx.tv code is under bsd license
__author__="Andrea Fassina"
__date__ ="$01-Oct-2014 23:34:47$"
from api import YouTube
import os,sys, traceback
from termcolor import colored
import json
import pprint
import re
import importlib
from db import DB
from datetime import datetime

#CONFIG HOOKS
CONFIG_NAME = "HMDYKA_CONFIG"
USE_CSV = "USE_CSV"
DOWNLOAD_YOUTUBE = "DOWNLOAD_YOUTUBE"
VIDEO_QUERY = "VIDEO_QUERY"
SPLIT_QUERY = "split_query"
DOWNLOAD_TEXT = "DOWNLOAD_TEXT"
MAX_VIDEOS_PER_EVENT = "MAX_VIDEOS_PER_EVENT"
ROOT = "ROOT"
MIN_SCORE = "MIN_SCORE"
DURATION = "DURATION"
MAX_DURATION = "MAX_DURATION"

# SCORING CONFIG
MOVIE_IN_TITLE="MOVIE_IN_TITLE"
DATE_IN_TITLE="DATE_IN_TITLE"
STARRING_IN_TITLE="STARRING_IN_TITLE"
WRITER_IN_TITLE="WRITER_IN_TITLE"
ITEM_IN_DESCRIPTION="ITEM_IN_DESCRIPTION"
ITEM_IN_TAGS="ITEM_IN_TAGS"
HAS_MANY_DATES="HAS_MANY_DATES"
MAX_MANY_DATES="MAX_MANY_DATES"
MIN_VIEW_COUNT="MIN_VIEW_COUNT"
HAS_MIN_VIEW_COUNT="HAS_MIN_VIEW_COUNT"
MIN_DURATION="MIN_DURATION"
MAX_DURATION="MAX_DURATION"
SCORE_DURATION="SCORE_DURATION"
SCORE_NO_DATE="SCORE_NO_DATE"
VIDEO_KEYWORDS="VIDEO_KEYWORDS"
VIDEO_IN_KEYWORD="VIDEO_IN_KEYWORD"
MINIMUM_TITLE_LENGTH="MINIMUM_TITLE_LENGTH"
MINIMUM_TITLE_LENGTH_SCORE="MINIMUM_TITLE_LENGTH_SCORE"


JSON_FILE_NAME = ""

DEFAULTS = {
    "init_date":1950,
    "end_date": 2020,
    "inc": 10,
    "lang": "it",
    "limit": 50,
    "MAX_DURATION":999999999999999999999,
    "DURATION": 0,
    "MAX_VIDEOS_PER_EVENT": 2,
    "MIN_SCORE":0
}
REQUIRED = ["MIN_SCORE","MOVIE_IN_TITLE", "DATE_IN_TITLE","MAX_VIDEOS_PER_EVENT", "MIN_VIEW_COUNT"]

#from api.categories import Film

CATEGORIES = {
    "film": "Film",
    "videogames": "Videogames",
    "football"  : "Football",
    "cartoons"  : "Cartoons",
    "history"   : "Politics"
}


class HMDYKA(object):
    def __init__(self, category, fn = "" ):
        """"""
        
        
        self.youtube = YouTube.SearchVideos()
        self.config = self.__getInput(fn = fn)
        self.fileName = self.__checkRequired()
        
        if CATEGORIES[category]:
            module = importlib.import_module('api.categories.'+CATEGORIES[category])
            my_class = getattr(module, CATEGORIES[category])
            print "Scoring video by: %s" % category
            self.config['CATEGORY'] = category
            self.score_movie = my_class(self.config)
            self.score_movie.config = self.config
            self.category = category
            self.fileName += "_" + self.category
            self.db = DB(category)

        else:
            print "Category %s not found. Exiting" % category
            sys.exit(2)
        
        



    def __checkRequired(self):
        """Checks required values in configuration"""
        fn = ""
        for r in REQUIRED:
            if r not in self.config:
                self.config[r] = DEFAULTS[r]
                print "MISSING REQUIRED VALUE %s , using default: %s" %(r, DEFAULTS[r])
            else:
                fn += self.config[r]+"_"
        fn = fn.replace('/','')
        return "data/"+fn


    def __getInput(self, fn = ""):
        """Reads config file in outputs dict of var=value"""
        if len(fn)==0:
            fn = CONFIG_NAME
        file = open(fn, 'r')
        lines = file.readlines()
        config = {}
        for line in lines:
            if not "#" in line:
                l = line.split("=")
                try:
                    key = l[0].strip()
                    value = l[1].strip()
                    config[key]=value
                except:
                    pass
        return config


    def _runDBPediaQuery(self):
        """"""

    def _run(self):
        results = []
        if DOWNLOAD_TEXT in self.config:
            results = self._runDBPediaQuery()
        if DOWNLOAD_YOUTUBE in self.config:

            # Read data
            items = self._readDataFromJSON("/".join(self.fileName.split('/')[:-1])+"/"+JSON_FILE_NAME)

            queries = self.score_movie.convertDictToQuery(items)
            pprint.pprint("Generated %s queries\nSample: %s" % (len(queries), queries[0]))
        elif VIDEO_QUERY in self.config:
            queries = self.config[VIDEO_QUERY]
        else:
            print "Please decide what to search using the VIDEO_QUERY='something something' or USE_CSV with a suitable CSV file, in CONFIG file."
            sys.exit(2)
        return self.searchFeed(queries)
    


    def getVideoClips(self):
        """Retrieves video clips.

        Add to video metadata:
        `feed[X]` is the tuple of entry [0] and [1] query information
        `entry = feed[tot][0]`
        `q = feed[tot][1][0]`
        `eventName = feed[tot][1][1]`
        `date = feed[tot][1][2]`
        """

        try:
            ##### RUN YOUTUBE QUERY
            results = self._run()
            return results
        except Exception as e:
            print "[getVideoClips] ERROR: %s" % e
            exc_type, exc_value, exc_traceback = sys.exc_info()
            error = " Error whilst running HOST:\n\n%s" % traceback.format_exc(limit=10)
            print error
            sys.exit(2)

    def hasRequired(self, item, feed, required_key = "title"):
        """Check if it has required key"""
        ids = [] 
        yt_prefix = 'http://www.youtube.com/embed/'
        for entry in feed:
            if item[required_key].lower() in entry['snippet']['title'].lower() or item[required_key].lower() in entry['snippet']['description'].lower():
                save = True
                try:
                    if self.db.exists(yt_prefix+entry['id']):
                        save = False
                except:
                    pass                        
                try:
                    if save:
                        ids.append(entry['id']['videoId'])
                except:
                    pass   
        return ids    

        
    def _scoreFeed(self, feed, query, required_key = "title"):
        """
        Analyse feed.
        
        """
        # Populated from the query creation
        o = 0
        videos = []  
        item = query[1]
        try:
            date = query[1]['date']
        except:
            return
        category = query[2]
        try:
            root = query[2].split('_')[1]
        except:
            root = query[3]
        q = query[0]
        # General checks
        try:
            ids = self.score_movie.hasRequired(item, feed, q )
        except Exception as e:
            ids = self.hasRequired(item,feed, required_key)
        # videos with more info
        feed_with_info = self.youtube.getVideo(id_video=ids)

        for yt_vid in feed_with_info:
            videos.append(self.youtube.createVideoObject(
                    entry = yt_vid,
                    item = item,
                    q = q,
                    date = date,
                    root = root,
                    category = self.category
            ))
        vids = []
        top_vids = []
        #print "Total videos from query: %s"%len(videos)
        for video in videos:
            # SCORING OF VIDEO HAPPENS HERE
            video.score = self._scoreVideo(video)
            min_score = int(self.config[MIN_SCORE])
            try:
                words_in_title = [word for word in video.item['title'].split(' ') if len(word) > 2]
                if len(words_in_title) <= int(self.config[MINIMUM_TITLE_LENGTH]):
                    min_score = int(self.config[MINIMUM_TITLE_LENGTH_SCORE])
            except:
                pass
            if int(video.score)>min_score:      
            #    print colored("\nVideo: %s\nQuery: %s\nScore: %s" % (video.title, video.query, video.score) , "green")
                vids.append((video,int(video.score)))
            #else:
            #    print "Video: %s\nQuery: %s\nScore: %s\n" % (video.title, video.query, video.score)
        t = sorted(vids,key=lambda x: x[1])
        for v in t:
            if len(top_vids)<int(self.config[MAX_VIDEOS_PER_EVENT]):
                top_vids.append(v)
        o = self._saveJSONWithResult(top_vids) #Save video to json and db
        s = self._saveResultsToDB(top_vids) # only saves the first video
        print colored("Video was saved: %s" % s, 'blue')
        return o

            
    def _scoreVideo(self, video):
        """Scores a video based on different parameters."""
        score = 0
        hasDate = False
        # Check if item is in video title
        try:
            if video.item['title'].lower() in video.title.lower():
                score += int(self.config["MOVIE_IN_TITLE"])
            else: 
                score -= int(self.config["MOVIE_IN_TITLE"])
        except:
            pass
        if video.item['date'] in video.title:
            score += int(self.config["DATE_IN_TITLE"])
            hasDate = True
        # Check that there are no other dates....
        try:
            hasManyDates = False
            search_dates = r"(?:(?:19|20)[0-9]{2})"
            dates_in_title = re.findall(search_dates, video.title)
            dates_in_description = re.findall(search_dates, video.description)
            if len([dt for dt in dates_in_title if not dt==video.date]) > int(self.config[MAX_MANY_DATES]): hasManyDates = True
            if len([dt for dt in dates_in_description if not dt==video.date]) > int(self.config[MAX_MANY_DATES]): hasManyDates = True
            #print "[_scoreVideo] hasManyDates : %s " % hasManyDates
            if hasManyDates:
                score -= int(self.config["HAS_MANY_DATES"])
        except Exception as e:
            print "[_scoreVideo] ERROR in checking many dates: %s" % e
        if not ((video.duration > int(self.config["MIN_DURATION"])) and (video.duration < int(self.config["MAX_DURATION"]))):
            score = - int(self.config["SCORE_DURATION"])
        if int(video.viewcount) >= int(self.config["MIN_VIEW_COUNT"]):
            score += int(self.config["HAS_MIN_VIEW_COUNT"])
        else:
            score -= int(self.config["HAS_MIN_VIEW_COUNT"])
        if video.date in video.description:
            hasDate = True
        print "4: %s" % score
        # As of now the video has been checked for: duration, view count, date in title, many dates in title
        # Check if video item is in description
        # Is it a good thing to check here?
        # Doesnt take into account nested ditctionaries
        try:
            check = self.score_movie.checkItemsInVideo
        except:
            check = self.checkItemsInVideo
        for item_key, item_val in video.item.iteritems():
            if isinstance(item_val, list):
                for entry in item_val: 
                    score += check(entry, video)
            else:
                score += check(item_val,video)
        print "5: %s" % score
        try:
            keywords = self.config["VIDEO_KEYWORDS"].split(',')
            for keyword in keywords:
                if not (keyword in video.item['title']):
                    k = keyword.lower()
                    if (k in video.title.lower()) or (k in video.description.lower()) or (k in video.tags):
                        score = -int(self.config["VIDEO_IN_KEYWORD"])
        except:
            pass
        try:
            score += self.score_movie.checkTitleAndDescriptionWordLength(score,video)
        except:
            score += self.checkTitleAndDescriptionWordLength(score,video)
        # Set score temporarily
        video.score = score
        if hasDate is False:
            # Video fails if no date?
            score = -int(self.config["SCORE_NO_DATE"])
        try:
            score = self.scoreByCategory(video)
        except Exception as e:
            print "[_scoreVideo] ERROR in category scoring: %s" % e
        print "Score: %s" % score
        return score

    def checkTitleAndDescriptionWordLength(self, score, video):
        if score > int(self.config["MIN_SCORE"]):
            # Weight in length of title
            words_in_title = [word for word in video.title.split(' ') if len(word) > 2]
            words_in_description = [word for word in video.description.split(' ') if len(word) > 2]
            score += (len(words_in_title) * int(self.config["SCORE_TITLE_LENGTH"]))
            score += (len(words_in_description) * int(self.config["SCORE_DESCRIPTION_LENGTH"]))     
            return score
        else:
            return 0                     

    def checkItemsInVideo(self,item_val,video):
        score = 0
        if isinstance(item_val, dict):
            return score
        try:
            if item_val.lower() in video.description.lower():
                score += int(self.config["ITEM_IN_DESCRIPTION"])
            if item_val.lower() in [t.lower() for t in video.tags]:
                score += int(self.config["ITEM_IN_TAGS"])
        except Exception as e:
            print "[checkItemsInVideo] Error: %s" % e
            pass
        return score

    def scoreByCategory(self, video):
        """ Particular scoring for category"""        
        return self.score_movie.scoreVideo(video)

    def _saveResultsToDB(self, top_vids):
        """Saves selected videos in database, saves only first video in list."""        
        try:
            video_id = self.db.save(top_vids[0][0].__dict__)
            print colored("Saved: %s from %s with id: %s" % (top_vids[0][0].url, top_vids[0][0].query, video_id), "red")
            return True
        except Exception as e:
            print "[_saveResultsToDB] Error in saving video: %s" % e
            return False


    def _saveJSONWithResult(self, top_vids):
        """Saves top videos found for query in JSON file."""
        o = 0
        fn = self.fileName+"_youtube.json"
        for vi in top_vids:
            try:
                video_data = vi[0]
                #print colored(("SAVING %s for %s" % (video_data.title, video_data.query)), "red")
                try:
                    jsonFile = open(fn, "r")
                    data = json.load(jsonFile)
                    jsonFile.close()
                except:
                    data = []
                data.append(video_data.__dict__)
                jsonFile = open(fn, "w+")
                jsonFile.write(json.dumps(data, indent=4, sort_keys=True))
                jsonFile.close()
                o += 1
            except Exception as e:
                print "[_saveJSONWithResult] ERROR IN SAVING: %s" % e
                pass
        return o
    

    def _readDataFromJSON(self,fileName = ""):
        """
        Reads query searches from local file as a list.

        Return list with line information
        """
        with open(fileName, 'r') as handle:
            parsed = json.load(handle)
            print "[_readDataFromJSON] Read: %s rows." % len(parsed)
            return parsed

    def searchFeed(self, queries = []):
        """
        Search either a list of items or a string of text.

        Return list with results feeds.
        """
        feeds = []
        saved_total = 0
        processed_queries = []
        for query in queries:
            if not query[0] in processed_queries:
                feed = self.youtube.runYouTubeQuery(query = query[0])
                try:
                    saved_total+=self._scoreFeed(feed,query)
                    processed_queries.append(query[0])
                except Exception as e:
                    "The item was not found because: %s"  % e    
        return saved_total


if __name__ == "__main__":
    
    try:
        category = sys.argv[1]
    except:
        print "Category is needed to run!"
        sys.exit(2)
    try:
        JSON_FILE_NAME = sys.argv[2]
    except:
        JSON_FILE_NAME = "_%s.json" % category
    start = datetime.now().time()
    hm = HMDYKA(category)
    hm.getVideoClips()
    finish = datetime.now().time()
    print "Took %s to process %s in %s" % ((finish-start), category, JSON_FILE_NAME)

    

